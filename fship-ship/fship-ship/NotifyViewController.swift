//
//  NotifyViewController.swift
//  fship-ship
//
//  Created by vhcsoft on 4/11/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class NotifyViewController: UIViewController {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Danh sách thông báo"
        loadMenuButton(self, menuButton)
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
