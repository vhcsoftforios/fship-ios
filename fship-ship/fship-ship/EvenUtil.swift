//
//  EvenUtil.swift
//  fship-shop
//
//  Created by vhcsoft on 3/9/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import Foundation
import UIKit

// Thong bao
func alert(mySelf: UIViewController, title: String, message: String) {
    if let getModernAlert: AnyClass = NSClassFromString("UIAlertController") { // iOS 8
        let myAlert: UIAlertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        myAlert.addAction(UIAlertAction(title: "Đồng ý", style: .Default, handler: nil))
        mySelf.presentViewController(myAlert, animated: true, completion: nil)
    } else { // iOS 7
        let alert: UIAlertView = UIAlertView()
        alert.delegate = mySelf
        
        alert.title = title
        alert.message = message
        alert.addButtonWithTitle("Đồng ý")
        
        alert.show()
    }
}

// Menu Left
func loadMenuButton(mySelf: UIViewController, menuButton: UIBarButtonItem){
    if mySelf.revealViewController() != nil {
        menuButton.target = mySelf.revealViewController()
        menuButton.action = "revealToggle:"
        mySelf.view.addGestureRecognizer(mySelf.revealViewController().panGestureRecognizer())
    }
}

//
extension String
{
    func replace(target: String, withString: String) -> String
    {
        return self.stringByReplacingOccurrencesOfString(target, withString: withString, options: NSStringCompareOptions.LiteralSearch, range: nil)
    }
}

