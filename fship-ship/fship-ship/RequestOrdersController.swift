//
//  RequestOrdersController.swift
//  fship-ship
//
//  Created by vhcsoft on 4/11/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class RequestOrdersController: UIViewController {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Đơn hàng yêu cầu ship"
        loadMenuButton(self, menuButton)
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
