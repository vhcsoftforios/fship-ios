//
//  ChangePasswordController.swift
//  fship-shop
//
//  Created by vhcsoft on 3/19/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class ChangePasswordController: UIViewController {

    @IBOutlet weak var txtPasswordOld: UITextField!
    @IBOutlet weak var txtPasswordNew: UITextField!
    @IBOutlet weak var txtRetypePassword: UITextField!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    var username = ""
    var password = ""
    
    var passwordOld = ""
    var passwordNew = ""
    var retypePassword = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Đổi mật khẩu"
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        loadMenuButton(self, menuButton)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func btnXacNhan(sender: UIButton) {
        passwordOld = txtPasswordOld.text
        passwordNew = txtPasswordNew.text
        retypePassword = txtRetypePassword.text
        if (
            passwordOld.isEqual("") ||
            passwordNew.isEqual("") ||
            retypePassword.isEqual("")
        )
        {
            SCLAlertView().showNotice(self, title: TITLE_ALERT_APP, subTitle: message_not_full_information, closeButtonTitle: title_button_ok, duration: 0.0)
        }
        else {
            let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
            username = prefs.valueForKey("USERNAME") as NSString
            password = prefs.valueForKey("PASSWORD") as NSString
            
            if(!passwordNew.isEqual(retypePassword))
            {
                SCLAlertView().showError(self, title: TITLE_ALERT_APP, subTitle: "Nhập lại mật khẩu không đúng", closeButtonTitle: title_button_ok, duration: 0.0)
            }
            else
            {
                if(!passwordOld.isEqual(password))
                {
                    SCLAlertView().showError(self, title: TITLE_ALERT_APP, subTitle: "Mật khẩu cũ không đúng", closeButtonTitle: title_button_ok, duration: 0.0)
                }
                else
                {
                    SwiftLoader.show(title: title_loading, animated: true)
                    apiChangePassword(passwordNew)
                }
            }
        }
    }
    
    func apiChangePassword(passwordNew: String){
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        
        // create some JSON data and configure the request
        let jsonString = "{\"newPassword\": \"\(passwordNew)\"}"
        let url = NSURL(string: SERVER_URL + API_CHANGE_PASSWORD_SHOP)
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        var err: NSError?
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttp)

    }
    
    func getHttp(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                println("response was not 200: \(res)")
            }
            else{

                NSUserDefaults.standardUserDefaults().setObject(passwordNew, forKey: "PASSWORD")
                NSUserDefaults.standardUserDefaults().synchronize()
                let name = NSUserDefaults.standardUserDefaults().stringForKey("PASSWORD")
                println(httpResponse.statusCode)
                
                SCLAlertView().showSuccess(self, title: TITLE_ALERT_APP, subTitle: "Thay đổi mật khẩu thành công.", closeButtonTitle: title_button_ok, duration: 0.0)
                NSLog("Change Password SUCCESS");
                return
            }
        }
        
        SCLAlertView().showError(self, title: TITLE_ALERT_APP, subTitle: "Thay đổi mật khẩu thất bại.", closeButtonTitle: title_button_ok, duration: 0.0)
        if (error != nil) {
            println("error submitting request: \(error)")
            return
        }
    }
}
