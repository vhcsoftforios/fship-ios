//
//  OrdersStatus0Cell.swift
//  fship-shop
//
//  Created by vhcsoft on 3/19/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit
/*
Function: Quan ly don hang trang thai goi ship
Description:
Create Date: 23/03/2015
Create By: QuangBui
*/
class OrdersStatus0Cell: UITableViewCell {

    @IBOutlet weak var lblCodeBill: UILabel!
    @IBOutlet weak var lblCostProduct: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblCostShipShop: UILabel!
    @IBOutlet weak var lblCountShipper: UILabel!
    @IBOutlet weak var imageDelete: UIImageView!
    @IBOutlet weak var imageShipper: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setStatus0Cell(record: SysBill)
    {
        self.lblCodeBill.text = record.codeBill
        self.lblCostProduct.text = record.costProduct
        self.lblDistance.text = "( \(record.distance) Km)"
        self.lblCostShipShop.text = record.costShipShop
        self.lblCountShipper.text = String(record.countShipper)
    }
}
