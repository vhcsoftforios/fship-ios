//
//  PopUpViewDetailOrders.swift
//  fship-shop
//
//  Created by vhcsoft on 3/25/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class PopUpViewDetailOrders: UIViewController {

    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var lblCodeBill: UILabel!
    @IBOutlet weak var lblAddressFrom: UILabel!
    @IBOutlet weak var lblAddressTo: UILabel!
    @IBOutlet weak var lblDeadline: UILabel!
    @IBOutlet weak var lblPhoneCustomer: UILabel!

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.shadowOpacity = 0.8
        self.popUpView.layer.shadowOffset = CGSizeMake(0.0, 0.0)
    }
    
    func showInView(
        aView: UIView!,
        withCodeBill codeBill: String!,
        withAddressFrom addressFrom : String!,
        withAddressTo addressTo: String!,
        withDeadline deadline: String!,
        withPhoneCustomer phoneCustomer: String!,
        animated: Bool)
    {
        aView.addSubview(self.view)
        lblCodeBill!.text = codeBill
        lblAddressFrom!.text = addressFrom
        lblAddressTo!.text = addressTo
        lblDeadline!.text = deadline
        lblPhoneCustomer!.text = phoneCustomer
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    @IBAction func closePopup(sender: AnyObject) {
        self.removeAnimate()
    }
}
