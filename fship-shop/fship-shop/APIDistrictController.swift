//
//  APIDistrictController.swift
//  fship-shop
//
//  Created by vhcsoft on 4/2/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import Foundation

protocol APIDistrictControllerProtocol {
    func didReceiveAPIDistrictResults(results: NSArray)
}

protocol APIDistrictFromControllerProtocol {
    func didReceiveAPIDistrictFromResults(results: NSArray)
}

protocol APIDistrictToControllerProtocol {
    func didReceiveAPIDistrictToResults(results: NSArray)
}

class APIDistrictController {
    
    var delegate: APIDistrictControllerProtocol?
    var delegateFrom: APIDistrictFromControllerProtocol?
    var delegateTo: APIDistrictToControllerProtocol?
    
    init() {
    }
    
    func loadDataDistrictByKey(key: String) -> Void {
        var request = HTTPTask()
        request.GET(SERVER_URL + API_DISTRICT, parameters: ["province": key], success: {(response: HTTPResponse) in
            if let data = response.responseObject as? NSData {
                let str = NSString(data: data, encoding: NSUTF8StringEncoding)
                
                var parseError: NSError?
                let responseObject = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &parseError) as NSArray
                self.delegate?.didReceiveAPIDistrictResults(responseObject)
                //println(responseObject)
                
            }
            },failure: {(error: NSError, response: HTTPResponse?) in
                println("error: \(error)")
        })
        
    }
    
    func loadDataDistrictFromByKey(key: String) -> Void {
        var request = HTTPTask()
        request.GET(SERVER_URL + API_DISTRICT, parameters: ["province": key], success: {(response: HTTPResponse) in
            if let data = response.responseObject as? NSData {
                let str = NSString(data: data, encoding: NSUTF8StringEncoding)
                
                var parseError: NSError?
                let responseObject = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &parseError) as NSArray
                self.delegateFrom?.didReceiveAPIDistrictFromResults(responseObject)
                //println(responseObject)
                
            }
            },failure: {(error: NSError, response: HTTPResponse?) in
                println("error: \(error)")
        })
        
    }
    
    func loadDataDistrictToByKey(key: String) -> Void {
        var request = HTTPTask()
        request.GET(SERVER_URL + API_DISTRICT, parameters: ["province": key], success: {(response: HTTPResponse) in
            if let data = response.responseObject as? NSData {
                let str = NSString(data: data, encoding: NSUTF8StringEncoding)
                
                var parseError: NSError?
                let responseObject = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &parseError) as NSArray
                self.delegateTo?.didReceiveAPIDistrictToResults(responseObject)
                //println(responseObject)
                
            }
            },failure: {(error: NSError, response: HTTPResponse?) in
                println("error: \(error)")
        })
        
    }
}