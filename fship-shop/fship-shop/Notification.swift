//
//  Notification.swift
//  fship-shop
//
//  Created by vhcsoft on 3/16/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import Foundation

class Notification
{
    var id = 0
    var codeBill = "codeBill"
    var userFrom = "userFrom"
    var userTo = "userTo"
    var messageType = "messageType"
    var message = "message"
    
    init(id: Int, codeBill: String, userFrom: String, userTo: String, messageType: String, message: String)
    {
        self.id = id
        self.codeBill = codeBill
        self.userFrom = userFrom
        self.userTo = userTo
        self.messageType = messageType
        self.message = message
    }
}