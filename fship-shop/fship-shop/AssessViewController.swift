//
//  AssessViewController.swift
//  fship-shop
//
//  Created by vhcsoft on 3/26/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//
/*
Function: Danh gia Shipper
Description:
Create Date: 26/03/2015
Create By: QuangBui
*/
import UIKit

class AssessViewController: UIViewController, UITextViewDelegate, UITableViewDataSource, UITableViewDelegate, APICommentShipperControllerProtocol {

    @IBOutlet weak var barButtonBack: UIBarButtonItem!
    @IBOutlet weak var commentTableView: UITableView!
    @IBOutlet weak var lblShipperName: UILabel!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblLikeNumber: UILabel!
    @IBOutlet weak var lblDislikeNumber: UILabel!
    @IBOutlet weak var txtComment: UITextView!
    @IBOutlet weak var lblPointNumber: UILabel!
    var shipperName: String?
    var arrayOfCommentShipper: [Assessment] = [Assessment]()
    var apiCommentShipper = APICommentShipperController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Đánh giá Shipper"
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        barButtonBack.target = self
        barButtonBack.action = "clickBackLogin:"
        loadTextView()
        getAPIShipperInfo(shipperName!)
        
        var nib = UINib(nibName: "CommentShipperCell", bundle: nil)
        commentTableView.registerNib(nib, forCellReuseIdentifier: "tvcCommentShipperCell")
        
        apiCommentShipper.loadDataCommentShipper(shipperName!)
        apiCommentShipper.delegate = self
        
        commentTableView.delegate = self
        commentTableView.dataSource = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func clickBackLogin(sender: UIBarButtonItem!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btnComment(sender: UIButton) {
        var content:NSString = txtComment.text
        if ( content.isEqualToString("") || content.isEqualToString("Bình luận về Shipper...")) {
            SCLAlertView().showNotice(self, title: TITLE_ALERT_APP, subTitle: "Vui lòng nhập bình luận.", closeButtonTitle: title_button_ok, duration: 0.0)
        }
        else {
            SwiftLoader.show(title: title_loading, animated: true)
            self.setCommentShipper(shipperName!, content: content)
        }
    }
    
    //================================= Text View
    func loadTextView(){
        txtComment.delegate = self
        if (txtComment.text == "") {
            textViewDidEndEditing(txtComment)
        }
        var tapDismiss = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        self.view.addGestureRecognizer(tapDismiss)
    }
    
    func dismissKeyboard(){
        txtComment.resignFirstResponder()
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if (textView.text == "") {
            textView.text = "Bình luận về Shipper..."
            textView.textColor = UIColor.lightGrayColor()
        }
        textView.resignFirstResponder()
    }
    
    func textViewDidBeginEditing(textView: UITextView){
        if (textView.text == "Bình luận về Shipper..."){
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
        textView.becomeFirstResponder()
    }
    //=================================
    //=================================
    func getAPIShipperInfo(shipperName: String) -> Void {
        SwiftLoader.show(title: title_loading, animated: true)
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var username:NSString = prefs.valueForKey("USERNAME") as NSString
        var password:NSString = prefs.valueForKey("PASSWORD") as NSString
        
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        
        let url = NSURL(string: SERVER_URL + API_GET_SHIPER_INFO.replace("{0}", withString: shipperName))
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "GET"
        var err: NSError?
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttpShipperInfo)
        
    }
    
    func getHttpShipperInfo(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                println("response was not 200: \(res)")
                return
            }
            else{
                var myData:NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                println(myData)
                
                var parseError: NSError?
                
                var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
                println("jsonResult: \(jsonResult)")
                
                let username = jsonResult["username"]! as String
                let avartarLocation = jsonResult["avartarLocation"]! as? String
                var likeNumber = jsonResult["likeNumber"]! as Int
                var dislikeNumber = jsonResult["dislikeNumber"]! as Int
                var pointNumber = jsonResult["pointNumber"]! as Int
                
                // Avatar
                let url = NSURL(string: avartarLocation!)
                let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
                self.imgAvatar.image = UIImage(data: data!)
                //
                lblShipperName.text = username
                lblLikeNumber.text = String(likeNumber)
                lblDislikeNumber.text = String(dislikeNumber)
                lblPointNumber.text = String(pointNumber)
                
                return
            }
        }
        if (error != nil) {
            println("error submitting request: \(error)")
            return
        }
    }
    
    //===============================================
    func didReceiveAPICommentShipperResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            var arrayResult = [Assessment]()
            for element in resultsArr {
                let id = element["id"]! as Int
                let userFrom = element["userFrom"]! as? String
                let userTo = element["userTo"]! as? String
                let content = element["content"]! as? String
                let createdDate = element["createdDate"]! as? Int
                
                var record = Assessment(id: id, userFrom: userFrom?, userTo: userTo?, content: content?, createdDate: createdDate?)
                
                arrayResult.append(record)
            }
            self.arrayOfCommentShipper = arrayResult
            
            self.commentTableView!.reloadData()
        })
    }
    
    //===============================================
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfCommentShipper.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: CommentShipperCell = tableView.dequeueReusableCellWithIdentifier("tvcCommentShipperCell") as CommentShipperCell
        
        var record: Assessment = arrayOfCommentShipper[indexPath.row]
        cell.setCommentCell(record)
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.commentTableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70.0
    }
    
    //===================Them nhat xet danh gia============================
    func setCommentShipper(userTo: String, content: String){
        
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var username:NSString = prefs.valueForKey("USERNAME") as NSString
        var password:NSString = prefs.valueForKey("PASSWORD") as NSString
        
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        let jsonString = "{\"userFrom\": \"\(username)\",\"userTo\": \"\(userTo)\",\"content\": \"\(content)\"}"
        
        let url = NSURL(string: SERVER_URL + API_UPDATE_COMMENT_INFO)
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        var err: NSError?
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttp)
        
    }
    
    func getHttp(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                println("response was not 200: \(res)")
                return
            }
            else{
                var myData:NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                println(myData)
                apiCommentShipper.loadDataCommentShipper(shipperName!)
                apiCommentShipper.delegate = self
                txtComment.text == ""
                loadTextView()
                SCLAlertView().showSuccess(self, title: TITLE_ALERT_APP, subTitle: "Thêm bình luận thành công.", closeButtonTitle: title_button_ok, duration: 0.0)
                
            }
        }
        if (error != nil) {
            println("error submitting request: \(error)")
            return
        }
        
    }
    
}
