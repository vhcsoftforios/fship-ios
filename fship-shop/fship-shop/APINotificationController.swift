//
//  APINotificationController.swift
//  fship-shop
//
//  Created by vhcsoft on 3/16/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import Foundation

protocol APINotificationControllerProtocol {
    func didReceiveAPINotificationResults(results: NSArray)
}

class APINotificationController {
    
    var delegate: APINotificationControllerProtocol?
    
    init() {
    }
    
    func loadDataNotification() -> Void {
        SwiftLoader.show(title: title_loading, animated: true)
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var username:NSString = prefs.valueForKey("USERNAME") as NSString
        var password:NSString = prefs.valueForKey("PASSWORD") as NSString
        
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        
        let url = NSURL(string: SERVER_URL + API_NOTIFICATION)
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "GET"
        var err: NSError?
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttp)
        
    }
    
    func getHttp(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            var parseError: NSError?
            let responseObject = NSJSONSerialization.JSONObjectWithData(data!, options: nil, error: &parseError) as NSArray
            self.delegate?.didReceiveAPINotificationResults(responseObject)
        }
        if (error != nil) {
            println("error request: \(error)")
            return
        }
    }
}