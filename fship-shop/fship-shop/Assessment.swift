//
//  Assessment.swift
//  fship-shop
//
//  Created by vhcsoft on 3/27/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import Foundation

class Assessment {
    var id: Int
    var userFrom: String?
    var userTo: String?
    var content: String?
    var createdDate: Int?

    init(id: Int, userFrom: String?, userTo: String?, content: String?, createdDate: Int?
        ) {
        self.id = id
        self.userFrom = userFrom
        self.userTo = userTo
        self.content = content
        self.createdDate = createdDate
    }
}