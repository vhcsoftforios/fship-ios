//
//  OrdersViewController.swift
//  fship-shop
//
//  Created by vhcsoft on 3/18/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//
/*
 Function: Quan ly don hang
 Description:
 Create Date: 19/03/2015
 Create By: QuangBui
*/
import UIKit

class OrdersViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, APIOrdersStatusControllerProtocol, UISearchBarDelegate {

    @IBOutlet weak var segmentOrders: UISegmentedControl!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var ordersTableView: UITableView!
    @IBOutlet weak var ordersSearchBar: UISearchBar!
    
    var searchActive : Bool = false
    var arrayOfOrderSysBill: [SysBill] = [SysBill]()
    var filtered:[SysBill] = [SysBill]()
    var apiSysBill = APIOrdersStatusController()
    var popViewController : PopUpViewDetailOrders!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Quản lý đơn hàng"
        loadMenuButton(self, menuButton)
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        
        loadOrdersStatus0Cell()
        ordersSearchBar.placeholder = "Tìm kiếm"
        ordersSearchBar.delegate = self
        ordersTableView.delegate = self // De map du lieu vao table
        ordersTableView.dataSource = self // De map du lieu vao table
        segmentOrders.selectedSegmentIndex = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filtered.count
        }
        return arrayOfOrderSysBill.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        
        if segmentOrders.selectedSegmentIndex == 0 // Trang thai GOI SHIP
        {
            let cell: OrdersStatus0Cell = tableView.dequeueReusableCellWithIdentifier("tvcOrdersStatus0Cell") as OrdersStatus0Cell
            var record: SysBill
            if(searchActive){
                record = filtered[indexPath.row]
                cell.setStatus0Cell(record)
            } else {
                record = arrayOfOrderSysBill[indexPath.row]
                cell.setStatus0Cell(record)
            }
            
            if(record.severity == tev_severity_value_fast)
            {
                cell.lblCodeBill?.textColor = UIColor.redColor()
            }
            if(record.severity == tev_severity_value_normal)
            {
                cell.lblCodeBill?.textColor = UIColor.blueColor()
            }
            
            // Click vao image delete
            cell.imageDelete?.userInteractionEnabled = true
            cell.imageDelete?.tag = indexPath.row
            var tapped1:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "TappedOnImageDelete:")
            tapped1.numberOfTapsRequired = 1
            cell.imageDelete?.addGestureRecognizer(tapped1)
            
            // Click vao image shipper nhan
            cell.imageShipper?.userInteractionEnabled = true
            cell.imageShipper?.tag = indexPath.row
            var tapped2:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "TappedOnImageShipper:")
            tapped2.numberOfTapsRequired = 1
            cell.imageShipper?.addGestureRecognizer(tapped2)
            return cell
        }
        else if segmentOrders.selectedSegmentIndex == 1 // Trang thai CHO SHIP
        {
            let cell: OrdersStatus1Cell = tableView.dequeueReusableCellWithIdentifier("tvcOrdersStatus1Cell") as OrdersStatus1Cell
            var record: SysBill
            if(searchActive){
                record = filtered[indexPath.row]
                cell.setStatus1Cell(record)
            } else {
                record = arrayOfOrderSysBill[indexPath.row]
                cell.setStatus1Cell(record)
            }
            
            if(record.severity == tev_severity_value_fast)
            {
                cell.lblCodeBill?.textColor = UIColor.redColor()
            }
            if(record.severity == tev_severity_value_normal)
            {
                cell.lblCodeBill?.textColor = UIColor.blueColor()
            }
            
            // Click vao image confirm
            cell.imageConfirm?.userInteractionEnabled = true
            cell.imageConfirm?.tag = indexPath.row
            var tapped1:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "TappedOnImageConfirm:")
            tapped1.numberOfTapsRequired = 1
            cell.imageConfirm?.addGestureRecognizer(tapped1)
            
            cell.imageDelete?.userInteractionEnabled = true
            cell.imageDelete?.tag = indexPath.row
            var tapped2:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "TappedOnImageDeleteStatus1:")
            tapped2.numberOfTapsRequired = 1
            cell.imageDelete?.addGestureRecognizer(tapped2)
            
            return cell
        }
        else if segmentOrders.selectedSegmentIndex == 2 // Trang thai DANG SHIP
        {
            let cell: OrdersStatus2Cell = tableView.dequeueReusableCellWithIdentifier("tvcOrdersStatus2Cell") as OrdersStatus2Cell
            
            var record: SysBill
            if(searchActive){
                record = filtered[indexPath.row]
                cell.setStatus2Cell(record)
            } else {
                record = arrayOfOrderSysBill[indexPath.row]
                cell.setStatus2Cell(record)
            }
            
            if(record.severity == tev_severity_value_fast)
            {
                cell.lblCodeBill?.textColor = UIColor.redColor()
            }
            if(record.severity == tev_severity_value_normal)
            {
                cell.lblCodeBill?.textColor = UIColor.blueColor()
            }
            
            // Click vao image confirm
            cell.imageConfirm?.userInteractionEnabled = true
            cell.imageConfirm?.tag = indexPath.row
            var tapped1:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "TappedOnImageConfirmStatus2:")
            tapped1.numberOfTapsRequired = 1
            cell.imageConfirm?.addGestureRecognizer(tapped1)
            
            // Click vao image hang bi tra lai
            cell.imageReturns?.userInteractionEnabled = true
            cell.imageReturns?.tag = indexPath.row
            var tapped2:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "TappedOnImageReturnsStatus2:")
            tapped2.numberOfTapsRequired = 1
            cell.imageReturns?.addGestureRecognizer(tapped2)
            
            return cell
        }
        else // Trang thai DA SHIP
        {
            let cell: OrdersStatus3Cell = tableView.dequeueReusableCellWithIdentifier("tvcOrdersStatus3Cell") as OrdersStatus3Cell
            var record: SysBill
            if(searchActive){
                record = filtered[indexPath.row]
                cell.setStatus3Cell(record)
            } else {
                record = arrayOfOrderSysBill[indexPath.row]
                cell.setStatus3Cell(record)
            }
            
            if(record.severity == tev_severity_value_fast)
            {
                cell.lblCodeBill?.textColor = UIColor.redColor()
            }
            if(record.severity == tev_severity_value_normal)
            {
                cell.lblCodeBill?.textColor = UIColor.blueColor()
            }
            // Click vao image like
            cell.imageLikeShip?.userInteractionEnabled = true
            cell.imageLikeShip?.tag = indexPath.row
            var tapped1:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "TappedOnImageLikeShip:")
            tapped1.numberOfTapsRequired = 1
            cell.imageLikeShip?.addGestureRecognizer(tapped1)
            
            // Click vao image dislike
            cell.imageDislikeShip?.userInteractionEnabled = true
            cell.imageDislikeShip?.tag = indexPath.row
            var tapped2:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "TappedOnImageDislikeShip:")
            tapped2.numberOfTapsRequired = 1
            cell.imageDislikeShip?.addGestureRecognizer(tapped2)
            
            // Click vao button shipper name
            cell.btnShipperName?.userInteractionEnabled = true
            cell.btnShipperName?.tag = indexPath.row
            var tapped3:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "TappedOnButtonShipperName:")
            tapped2.numberOfTapsRequired = 1
            cell.btnShipperName?.addGestureRecognizer(tapped3)
            
            return cell
        }
        
    }
    
    var codeBill = String()
    var index = Int?()
    //==================================GOI SHIP==========================================
    func TappedOnImageDelete(sender:UITapGestureRecognizer){
        println(sender.view?.tag)
        index = sender.view?.tag
        self.codeBill = arrayOfOrderSysBill[index!].codeBill!
        if(searchActive) {
            self.codeBill = filtered[index!].codeBill!
        }
        
        let alert = SCLAlertView()
        alert.addButton(title_button_ok) {
            self.deleteOrders(self.codeBill)
        }
        alert.showWarning(self, title: TITLE_ALERT_APP, subTitle: "Bạn có chắc chắn muốn xoá đơn hàng không?", closeButtonTitle: title_button_cancel, duration: 0.0)
    }
    
    func deleteOrders(codeBill: String) -> Void {
        SwiftLoader.show(title: title_loading, animated: true)
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var username:NSString = prefs.valueForKey("USERNAME") as NSString
        var password:NSString = prefs.valueForKey("PASSWORD") as NSString
        
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        
        let url = NSURL(string: SERVER_URL + API_DELETE_ORDER.replace("{0}", withString: codeBill))
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "DELETE"
        var err: NSError?
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttpDelete)
        
    }
    
    func getHttpDelete(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                println("response was not 200: \(res)")
                return
            }
            else{
                var myData:NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                println(myData)
                
                switch(segmentOrders.selectedSegmentIndex){
                case 0:
                    loadOrdersStatus0Cell()
                case 1:
                    loadOrdersStatus1Cell()
                case 2:
                    loadOrdersStatus2Cell()
                case 3:
                    loadOrdersStatus3Cell()
                default:
                    println("Error")
                }
                
                SCLAlertView().showSuccess(self, title: TITLE_ALERT_APP, subTitle: "Xoá đơn hàng thành công.", closeButtonTitle: title_button_ok, duration: 0.0)
                return
            }
        }
        if (error != nil) {
            println("error submitting request: \(error)")
            return
        }
    }
    
    func TappedOnImageShipper(sender:UITapGestureRecognizer){
        println(sender.view?.tag)
        var detailedViewController: ListShipperController = self.storyboard?.instantiateViewControllerWithIdentifier("ListShipperController") as ListShipperController
        let index = sender.view?.tag
        var record = arrayOfOrderSysBill[index!]
        if(searchActive) {
            record = filtered[index!]
        }
        detailedViewController.codeBill = record.codeBill
        
        let navigationController = UINavigationController(rootViewController: detailedViewController)
        self.presentViewController(navigationController, animated: true, completion: nil)
    }
    //=====================================================================================
    //==================================CHO SHIP===========================================
    func TappedOnImageConfirm(sender:UITapGestureRecognizer){
        println(sender.view?.tag)
        index = sender.view?.tag
        self.codeBill = arrayOfOrderSysBill[index!].codeBill!
        if(searchActive) {
            self.codeBill = filtered[index!].codeBill!
        }
        
        let alert = SCLAlertView()
        alert.addButton(title_button_ok) {
            self.changeStatusShip(self.codeBill, status: "2")
        }
        alert.showWarning(self, title: TITLE_ALERT_APP, subTitle: "Shipper đến nhận hàng thành công?", closeButtonTitle: title_button_cancel, duration: 0.0)
    }
    
    var status = String()
    func changeStatusShip(codeBill: String, status: String) -> Void {
        self.status = status
        SwiftLoader.show(title: title_loading, animated: true)
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var username:NSString = prefs.valueForKey("USERNAME") as NSString
        var password:NSString = prefs.valueForKey("PASSWORD") as NSString
        
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        let jsonString = "{\"status\": \"\(self.status)\",\"codeBill\": \"\(codeBill)\"}"
        
        let url = NSURL(string: SERVER_URL + API_UPDATE_STATUS_ORDER)
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        var err: NSError?
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttpConfirm)
        
    }
    
    func getHttpConfirm(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                println("response was not 200: \(res)")
                return
            }
            else{
                var myData:NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                println(myData)
                if self.status.isEqual("2"){
                    self.apiSysBill.loadDataOrdersStatus("1")
                    self.apiSysBill.delegate = self
                    self.ordersTableView!.reloadData()
                    SCLAlertView().showSuccess(self, title: TITLE_ALERT_APP, subTitle: "Chuyển trạng thái đơn hàng thành công.", closeButtonTitle: title_button_ok, duration: 0.0)
                }
                else if self.status.isEqual("3"){
                    self.apiSysBill.loadDataOrdersStatus("2")
                    self.apiSysBill.delegate = self
                    self.ordersTableView!.reloadData()
                    SCLAlertView().showSuccess(self, title: TITLE_ALERT_APP, subTitle: "Chuyển trạng thái đơn hàng thành công.", closeButtonTitle: title_button_ok, duration: 0.0)
                }
                else if self.status.isEqual("4"){
                    self.apiSysBill.loadDataOrdersStatus("2")
                    self.apiSysBill.delegate = self
                    self.ordersTableView!.reloadData()
                    SCLAlertView().showSuccess(self, title: TITLE_ALERT_APP, subTitle: "Đơn hàng đã trả lại.", closeButtonTitle: title_button_ok, duration: 0.0)
                }
                
                return
            }
        }
        if (error != nil) {
            println("error submitting request: \(error)")
            return
        }
    }
    
    func TappedOnImageDeleteStatus1(sender:UITapGestureRecognizer){
        index = sender.view?.tag
        self.codeBill = arrayOfOrderSysBill[index!].codeBill!
        if(searchActive) {
            self.codeBill = filtered[index!].codeBill!
        }
        
        let alert = SCLAlertView()
        alert.addButton("Xoá đơn hàng") {
            self.deleteOrders(self.codeBill)
        }
        alert.addButton("Gọi Shipper mới") {
            self.recallShipper(self.codeBill)
        }
        alert.showWarning(self, title: TITLE_ALERT_APP, subTitle: "Bạn muốn xoá đơn hàng hay gọi Shipper mới?", closeButtonTitle: title_button_cancel, duration: 0.0)
    }
    
    func recallShipper(codeBill: String) -> Void {
        SwiftLoader.show(title: title_loading, animated: true)
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var username:NSString = prefs.valueForKey("USERNAME") as NSString
        var password:NSString = prefs.valueForKey("PASSWORD") as NSString
        
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        
        let url = NSURL(string: SERVER_URL + API_RECALL_SHIPPER.replace("{0}", withString: codeBill))
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "GET"
        var err: NSError?
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttpRecallShipper)
        
    }
    
    func getHttpRecallShipper(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                println("response was not 200: \(res)")
                return
            }
            else{
                var myData:NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                println(myData)
                self.apiSysBill.loadDataOrdersStatus("1")
                self.apiSysBill.delegate = self
                self.ordersTableView!.reloadData()

                SCLAlertView().showSuccess(self, title: TITLE_ALERT_APP, subTitle: "Gọi lại Shipper thành công.", closeButtonTitle: title_button_cancel, duration: 0.0)
                return
            }
        }
        if (error != nil) {
            println("error submitting request: \(error)")
            return
        }
    }
    //=====================================================================================
    //==================================ĐANG SHIP==========================================
    func TappedOnImageConfirmStatus2(sender:UITapGestureRecognizer){
        index = sender.view?.tag
        self.codeBill = arrayOfOrderSysBill[index!].codeBill!
        if(searchActive) {
            self.codeBill = filtered[index!].codeBill!
        }
        
        let alert = SCLAlertView()
        alert.addButton(title_button_ok) {
            self.changeStatusShip(self.codeBill, status: "3")
        }
        alert.showWarning(self, title: TITLE_ALERT_APP, subTitle: "Shipper chuyển hàng thành công?", closeButtonTitle: title_button_cancel, duration: 0.0)
    }
    
    func TappedOnImageReturnsStatus2(sender:UITapGestureRecognizer){
        index = sender.view?.tag
        self.codeBill = arrayOfOrderSysBill[index!].codeBill!
        if(searchActive) {
            self.codeBill = filtered[index!].codeBill!
        }
        
        let alert = SCLAlertView()
        alert.addButton(title_button_ok) {
            self.changeStatusShip(self.codeBill, status: "4")
        }
        alert.showWarning(self, title: TITLE_ALERT_APP, subTitle: "Đơn hàng bị trả lại?", closeButtonTitle: title_button_cancel, duration: 0.0)
    }
    //=====================================================================================
    //==================================ĐÃ SHIP============================================
    func TappedOnImageLikeShip(sender:UITapGestureRecognizer){
        let cell: OrdersStatus3Cell = self.ordersTableView.dequeueReusableCellWithIdentifier("tvcOrdersStatus3Cell") as OrdersStatus3Cell
        cell.imageLikeShip?.userInteractionEnabled = true
        cell.imageLikeShip?.tag = sender.view!.tag
        
        index = sender.view?.tag
        var record: SysBill = arrayOfOrderSysBill[index!]
        if(searchActive) {
            record = filtered[index!]
        }
        
        if record.checkLikeShip == 1
        {
            self.likeAndDislikeShip( "1", codeBill: record.codeBill!, shipperName: record.shipperName?, rateLike: "0", rateDLike: "0")
        }
        else
        {
            self.likeAndDislikeShip( "1", codeBill: record.codeBill!, shipperName: record.shipperName?, rateLike: "1", rateDLike: "0")
        }
    }
    
    func TappedOnImageDislikeShip(sender:UITapGestureRecognizer){
        
        index = sender.view?.tag
        var record: SysBill = arrayOfOrderSysBill[index!]
        if(searchActive) {
            record = filtered[index!]
        }
        
        if record.checkLikeShip == 2
        {
            self.likeAndDislikeShip( "1", codeBill: record.codeBill!, shipperName: record.shipperName?, rateLike: "0", rateDLike: "0")
        }
        else
        {
            self.likeAndDislikeShip( "1", codeBill: record.codeBill!, shipperName: record.shipperName?, rateLike: "0", rateDLike: "1")
        }
    }
    
    // Click button Shipper danh gia Ship
    func TappedOnButtonShipperName(sender:UITapGestureRecognizer){
        
        index = sender.view?.tag
        var record: SysBill = arrayOfOrderSysBill[index!]
        if(searchActive) {
            record = filtered[index!]
        }
        
        var detailedViewController: AssessViewController = self.storyboard?.instantiateViewControllerWithIdentifier("AssessViewController") as AssessViewController
        detailedViewController.shipperName = record.shipperName
        
        let navigationController = UINavigationController(rootViewController: detailedViewController)
        self.presentViewController(navigationController, animated: true, completion: nil)
        
    }
    
    func likeAndDislikeShip(userType: String, codeBill: String, shipperName: String?, rateLike: String, rateDLike: String) -> Void {
        var jsonString = String()
        if shipperName != nil {
            let intString = String(shipperName!)
            jsonString = "{\"userType\": \(userType),\"codeBill\":\"\(codeBill)\", \"username\": \"\(intString)\",\"rateLike\": \(rateLike),\"rateDLike\": \(rateDLike)}"
        }
        else{
            jsonString = "{\"userType\": \(userType),\"codeBill\":\"\(codeBill)\", \"username\": \"\(shipperName)\",\"rateLike\": \(rateLike),\"rateDLike\": \(rateDLike)}"
        }
        
        SwiftLoader.show(title: title_loading, animated: true)
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var username:NSString = prefs.valueForKey("USERNAME") as NSString
        var password:NSString = prefs.valueForKey("PASSWORD") as NSString
        
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        //let jsonString = "{\"userType\": \(userType),\"codeBill\":\"\(codeBill)\", \"username\": \"\(intString)\",\"rateLike\": \(rateLike),\"rateDLike\": \(rateDLike)}"
        
        let url = NSURL(string: SERVER_URL + API_UPDATE_LIKE_INFO)
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        var err: NSError?
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttpLikeAndDislikeShip)
        
    }
    
    func getHttpLikeAndDislikeShip(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                println("response was not 200: \(res)")
                return
            }
            else{
                var myData:NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                println(myData)
                self.apiSysBill.loadDataOrdersStatus("3")
                self.apiSysBill.delegate = self
                self.ordersTableView!.reloadData()
                return
            }
        }
        if (error != nil) {
            println("error submitting request: \(error)")
            return
        }
    }
    //=====================================================================================
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var record = arrayOfOrderSysBill[indexPath.row]
        if(searchActive) {
            record = filtered[indexPath.row]
        }
        
        let myDateFormatter: NSDateFormatter = NSDateFormatter()
        myDateFormatter.dateFormat = "yyyy/MM/dd hh:mm"
        let date:NSDate = NSDate(timeIntervalSince1970:NSTimeInterval(record.deadline!))
        var deadline: NSString = myDateFormatter.stringFromDate(date)
        
        self.popViewController = PopUpViewDetailOrders(nibName: "popupDetailOrders", bundle: nil)
        self.popViewController.showInView(self.view, withCodeBill: record.codeBill, withAddressFrom: record.addressFrom, withAddressTo: record.addressTo, withDeadline: deadline, withPhoneCustomer: record.phoneCustomer, animated: true)
        
        self.ordersTableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func didReceiveAPIOrdersResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            var arrayResult = [SysBill]()
            for element in resultsArr {
                let id = element["id"]! as Int
                let codeBill = element["codeBill"]! as String
                let shopId = element["shopId"]! as? Int
                let shipperId = element["shipperId"]! as? Int
                let provinceFrom = element["provinceFrom"]! as? String
                let districtFrom = element["districtFrom"]! as? String
                let addressFrom = element["addressFrom"]! as? String
                let provinceTo = element["provinceTo"]! as? String
                let districtTo = element["districtTo"]! as? String
                let addressTo = element["addressTo"]! as? String
                let note = element["note"]! as? String
                let distance = element["distance"] as Double
                let costProduct = element["costProduct"]! as? String
                let costShipShop = element["costShipShop"]! as? String
                let costShipShipper = element["costShipShipper"]! as? String
                let countShipper = element["countShipper"]! as Int
                let severity = element["severity"]! as String
                let status = element["status"]! as Int
                let deadline = element["deadline"]! as? Int
                let phoneCustomer = element["phoneCustomer"]! as? String
                let latitude = element["latitude"]! as? String
                let longtitude = element["longtitude"]! as? String
                let shipperName = element["shipperName"]! as? String
                let sign = element["sign"]! as? String
                let shopName = element["shopName"]! as? String
                let checkLikeShip = element["checkLikeShip"]! as Int
                let checkLikeShop = element["checkLikeShop"]! as Int
                
                var record = SysBill(id: id, codeBill: codeBill, costProduct: costProduct?, severity: severity, shopId: shopId?, shipperId: shipperId?, provinceFrom: provinceFrom?, districtFrom: districtFrom?, addressFrom: addressFrom?, provinceTo: provinceTo?, districtTo: districtTo?, addressTo: addressTo?, note: note?, distance: distance, costShipShop: costShipShop?, costShipShipper: costShipShipper?, status: status, deadline: deadline?, phoneCustomer: phoneCustomer?, latitude: latitude?, longtitude: longtitude?, countShipper: countShipper, shipperName: shipperName?, sign: sign?, shopName: shopName?, checkLikeShip: checkLikeShip, checkLikeShop: checkLikeShop
                )
                arrayResult.append(record)
            }
            self.arrayOfOrderSysBill = arrayResult
            
            self.ordersTableView!.reloadData()
        })
    }
    
    //===================================================================================
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = arrayOfOrderSysBill.filter({ (text) -> Bool in
            let tmp: NSString = text.codeBill!
            let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.ordersTableView.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    //=====================================================================================
    @IBAction func segconChanged(sender: UISegmentedControl) {
        switch(sender.selectedSegmentIndex){
        case 0:
            loadOrdersStatus0Cell()
        case 1:
            loadOrdersStatus1Cell()
        case 2:
            loadOrdersStatus2Cell()
        case 3:
            loadOrdersStatus3Cell()
        default:
            println("Error")
        }
    }
    
    func loadOrdersStatus0Cell(){
        var nib = UINib(nibName: "OrdersStatus0Cell", bundle: nil)
        ordersTableView.registerNib(nib, forCellReuseIdentifier: "tvcOrdersStatus0Cell")
        self.apiSysBill.loadDataOrdersStatus("0")
        self.apiSysBill.delegate = self
        self.ordersTableView!.reloadData()
    }
    
    func loadOrdersStatus1Cell(){
        var nib = UINib(nibName: "OrdersStatus1Cell", bundle: nil)
        ordersTableView.registerNib(nib, forCellReuseIdentifier: "tvcOrdersStatus1Cell")
        self.apiSysBill.loadDataOrdersStatus("1")
        self.apiSysBill.delegate = self
        self.ordersTableView!.reloadData()
    }
    
    func loadOrdersStatus2Cell(){
        var nib = UINib(nibName: "OrdersStatus2Cell", bundle: nil)
        ordersTableView.registerNib(nib, forCellReuseIdentifier: "tvcOrdersStatus2Cell")
        self.apiSysBill.loadDataOrdersStatus("2")
        self.apiSysBill.delegate = self
        self.ordersTableView!.reloadData()
    }
    
    func loadOrdersStatus3Cell(){
        var nib = UINib(nibName: "OrdersStatus3Cell", bundle: nil)
        ordersTableView.registerNib(nib, forCellReuseIdentifier: "tvcOrdersStatus3Cell")
        self.apiSysBill.loadDataOrdersStatus("3")
        self.apiSysBill.delegate = self
        self.ordersTableView!.reloadData()
    }
}
