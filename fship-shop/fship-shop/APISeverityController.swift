//
//  APIProvinceController.swift
//  fship-shop
//
//  Created by vhcsoft on 4/2/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import Foundation

protocol APISeverityControllerProtocol {
    func didReceiveAPISeverityResults(results: NSArray)
}

class APISeverityController {
    
    var delegate: APISeverityControllerProtocol?
    
    init() {
    }
    
    func loadDataSeverity() -> Void {
        var request = HTTPTask()
        request.GET(SERVER_URL + API_SEVERITY_CONFIG, parameters: nil, success: {(response: HTTPResponse) in
            if let data = response.responseObject as? NSData {
                var parseError: NSError?
                let responseObject = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &parseError) as NSArray
                
                self.delegate?.didReceiveAPISeverityResults(responseObject)
                //println(responseObject)
            }
            },failure: {(error: NSError, response: HTTPResponse?) in
                println("error: \(error)")
        })
    }
    
}