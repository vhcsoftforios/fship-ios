//
//  SignupController.swift
//  fship-shop
//
//  Created by vhcsoft on 3/18/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class SignupController: UIViewController, APIProvinceControllerProtocol, APIDistrictControllerProtocol, UIPickerViewDelegate, UIToolbarDelegate {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var dropDownProvince: IQDropDownTextField!
    @IBOutlet weak var dropDownDistrict: IQDropDownTextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtRetypePassword: UITextField!
    @IBOutlet weak var barButtonBack: UIBarButtonItem!
    
    var apiProvince = APIProvinceController()
    var apiDistrict = APIDistrictController()
    //var arrayOfProvince: [Province] = [Province]()
    //var arrayOfDistrict: [District] = [District]()
    
    var resultKeysProvince = [String]()
    var resultValuesProvince = [String]()
    
    var resultKeysDistrict = [String]()
    var resultValuesDistrict = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = TITLE_SIGUP_PAGE
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        barButtonBack.target = self
        barButtonBack.action = "clickBackLogin:"
        
        apiProvince.loadDataProvince()
        apiProvince.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func clickBackLogin(sender: UIBarButtonItem!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func didReceiveAPIProvinceResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            //var arrayResult = [Province]()
            for element in resultsArr {
                let code = element["code"]! as? String
                let province = element["province"]! as? String
                
                self.resultKeysProvince.append(code!)
                self.resultValuesProvince.append(province!)
                
                //var record = Province(code: code?, province: province?)
                //arrayResult.append(record)
            }
            //self.arrayOfProvince = arrayResult
            
            self.dropDownProvince.setValue(self.resultValuesProvince, forKey: "itemList")
            self.apiDistrict.loadDataDistrictByKey(self.resultKeysProvince[0])
            self.apiDistrict.delegate = self
        })
    }
    
    func didReceiveAPIDistrictResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            //var arrayResult = [District]()
            self.resultKeysDistrict   = [String]()
            self.resultValuesDistrict   = [String]()
            
            for element in resultsArr {
                let district = element["district"]! as? String
                let districtName = element["districtName"]! as? String
                
                self.resultKeysDistrict.append(district!)
                self.resultValuesDistrict.append(districtName!)
                
                //var record = District(district: district?, districtName: districtName?)
                //arrayResult.append(record)
            }
            //self.arrayOfDistrict = arrayResult
            
            self.dropDownDistrict.setValue(self.resultValuesDistrict, forKey: "itemList")
        })
    }
    
    @IBAction func editingDidEndProvince(sender: AnyObject) {
        
        var index = dropDownProvince.selectedRow
        var province:NSString = dropDownProvince.text
        println("\(index) \(province)")
        if index >= 0 {
            self.apiDistrict.loadDataDistrictByKey(self.resultKeysProvince[index])
            self.apiDistrict.delegate = self
        }
    }
    
    // Su kien button Dang Ky
    @IBAction func singupTapped(sender: UIButton) {
        var username:NSString = txtUsername.text
        var password:NSString = txtPassword.text
        var phoneNumber:NSString = txtPhoneNumber.text
        var province:NSString = dropDownProvince.text
        var district:NSString = dropDownDistrict.text
        var address:NSString = txtAddress.text
        var retypePassword:NSString = txtRetypePassword.text
        println(self.dropDownProvince.text)
        
        if ( username.isEqualToString("")
            || password.isEqualToString("")
            || phoneNumber.isEqualToString("")
            || province.isEqualToString("")
            || district.isEqualToString("")
            || address.isEqualToString("")
            || retypePassword.isEqualToString("")
            ) {
                SCLAlertView().showNotice(self, title: TITLE_ALERT_APP, subTitle: message_not_full_information, closeButtonTitle: title_button_ok, duration: 0.0)
        }
        else{
            if(!password.isEqualToString(retypePassword)){
                SCLAlertView().showError(self, title: TITLE_ALERT_APP, subTitle: "Nhập lại mật khẩu không đúng", closeButtonTitle: title_button_ok, duration: 0.0)
            }else{
                SwiftLoader.show(title: title_loading, animated: true)
                registerRequest(username, password: password, phone: phoneNumber, province: province, district: district, address: address, note: "")
            }
        }
        
    }
    
    // Call API dang ky tai khoan
    func registerRequest(username: String, password: String, phone: String, province: String, district: String, address: String, note: String) -> Void{
        //create the request & response
        var request = NSMutableURLRequest(URL: NSURL(string: SERVER_URL + API_SIGUP_SHOPPER)!, cachePolicy: NSURLRequestCachePolicy.ReloadIgnoringLocalCacheData, timeoutInterval: 250)
        var response: NSURLResponse?
        var error: NSError?
        
        // create some JSON data and configure the request
        let jsonString = "{\"username\": \"\(username)\",\"password\": \"\(password)\",\"phone\": \"\(phone)\",\"province\": \"\(province)\",\"district\": \"\(district)\",\"address\":\"\(address)\",\"note\": \"\(note)\"}"
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttp)
    }
    
    func getHttp(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                println("response was not 200: \(res)")
            }
            else{
                
                var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
                
                let success:NSInteger = jsonResult.valueForKey("id") as NSInteger
                let sign:NSString = jsonResult.valueForKey("sign") as NSString
                
                var prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
                prefs.setObject(txtUsername.text, forKey: "USERNAME")
                prefs.setObject(txtPassword.text, forKey: "PASSWORD")
                prefs.setObject(sign, forKey: "SIGN")
                prefs.setObject(success, forKey: "ID")
                prefs.setInteger(1, forKey: "ISLOGGEDIN")
                prefs.synchronize()
                
                var detailedViewController: LoginController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginController") as LoginController
                detailedViewController.modalTransitionStyle = UIModalTransitionStyle.CoverVertical
                self.presentViewController(detailedViewController, animated: true, completion: nil)
                
                // Vao Main nhung k co thong bao nay
                SCLAlertView().showSuccess(self, title: TITLE_ALERT_APP, subTitle: "Đăng ký thành công.", closeButtonTitle: title_button_ok, duration: 0.0)
                NSLog("Register SUCCESS");
                return
            }
        }
        
        SCLAlertView().showError(self, title: TITLE_ALERT_APP, subTitle: "Đăng ký thất bại.", closeButtonTitle: title_button_ok, duration: 0.0)
        if (error != nil) {
            println("error submitting request: \(error)")
            return
        }
    }

}
