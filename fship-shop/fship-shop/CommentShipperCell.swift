//
//  CommentShipperCell.swift
//  fship-shop
//
//  Created by vhcsoft on 3/27/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class CommentShipperCell: UITableViewCell {

    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var lblDateComment: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setCommentCell(record: Assessment)
    {
        self.lblShopName.text = record.userFrom
        self.lblComment.text = record.content
        
        let myDateFormatter: NSDateFormatter = NSDateFormatter()
        myDateFormatter.dateFormat = "dd/MM/yyyy HH:mm:ss"
        let date:NSDate = NSDate(timeIntervalSince1970:NSTimeInterval(record.createdDate!))
        var createDate: NSString = myDateFormatter.stringFromDate(date)
        self.lblDateComment.text = createDate
        
    }
}
