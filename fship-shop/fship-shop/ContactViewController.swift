//
//  ContactViewController.swift
//  fship-shop
//
//  Created by vhcsoft on 3/19/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Liên hệ"
        loadMenuButton(self, menuButton)
        
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
}
