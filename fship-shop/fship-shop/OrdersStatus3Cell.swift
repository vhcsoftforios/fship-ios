//
//  OrdersStatus3Cell.swift
//  fship-shop
//
//  Created by vhcsoft on 3/23/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit
/*
Function: Quan ly don hang trang thai da ship
Description:
Create Date: 23/03/2015
Create By: QuangBui
*/
class OrdersStatus3Cell: UITableViewCell {

    @IBOutlet weak var lblCodeBill: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var imageLikeShip: UIImageView!
    @IBOutlet weak var imageDislikeShip: UIImageView!
    @IBOutlet weak var btnShipperName: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setStatus3Cell(record: SysBill)
    {
        self.lblCodeBill.text = record.codeBill
        self.lblDistance.text = "( \(record.distance) Km)"
        self.btnShipperName.setTitle(record.shipperName, forState: UIControlState.Normal)
        
        if record.status == 3 {
            self.lblMessage.text = "Hàng chuyển thành công"
            self.lblMessage?.textColor = UIColor.blueColor()
        }
        else if record.status == 4 {
            self.lblMessage.text = "Hàng bị trả lại"
            self.lblMessage?.textColor = UIColor.redColor()
        }
        
        switch(record.checkLikeShip){
        case 0:
            self.imageLikeShip!.image = UIImage(named: "like_false_32.png")
            self.imageDislikeShip!.image = UIImage(named: "dislike_false_32.png")
        case 1:
            self.imageLikeShip!.image = UIImage(named: "like_true_32.png")
            self.imageDislikeShip!.image = UIImage(named: "dislike_false_32.png")
        case 2:
            self.imageLikeShip!.image = UIImage(named: "like_false_32.png")
            self.imageDislikeShip!.image = UIImage(named: "dislike_true_32.png")
        default:
            println("Error")
        }
    }
    
}
