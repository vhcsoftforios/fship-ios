//
//  ListShipperCell.swift
//  fship-shop
//
//  Created by vhcsoft on 3/20/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class ListShipperCell: UITableViewCell {

    @IBOutlet weak var shipperName: UILabel!
    @IBOutlet weak var costShip: UILabel!
    @IBOutlet weak var phone: UILabel!
    @IBOutlet weak var note: UILabel!
    @IBOutlet weak var likeNumber: UILabel!
    @IBOutlet weak var dislikeNumber: UILabel!
    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var imageChecked: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setListShipperCell(record: Shipper)
    {
        self.shipperName.text = record.shipperName
        self.costShip.text = record.costShip
        self.phone.text = record.phone
        self.note.text = record.note
        self.likeNumber.text = String(Int(record.likeNumber!))
        self.dislikeNumber.text = String(Int(record.dislikeNumber!))
        
        let url = NSURL(string: record.avartarLocation!)
        let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
        self.imageAvatar.image = UIImage(data: data!)
    }
    
}
