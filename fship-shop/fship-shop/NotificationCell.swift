//
//  NotificationCell.swift
//  fship-shop
//
//  Created by vhcsoft on 3/18/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class NotificationCell: UITableViewCell {

    @IBOutlet weak var lblMessage: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func setNotificationCell(record: Notification)
    {
        self.lblMessage.text = record.message
    }
}
