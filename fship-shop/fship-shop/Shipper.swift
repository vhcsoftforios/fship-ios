//
//  Shipper.swift
//  fship-shop
//
//  Created by vhcsoft on 3/20/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import Foundation

class Shipper
{
    var id: Int
    var shipperId: Int?
    var shopId: Int?
    var status: Int?
    var codeBill: String?
    var costShip: String?
    var note: String?
    var shipperName: String?
    var phone: String?
    var avartarLocation: String?
    var likeNumber: Int?
    var dislikeNumber: Int?
    var pointNumber: Int?
    
    init(id: Int, shipperId: Int?, shopId: Int?, status: Int?, codeBill: String?, costShip: String?, note: String?, shipperName: String?, phone: String?, avartarLocation: String?, likeNumber: Int?, dislikeNumber: Int?, pointNumber: Int?)
    {
        self.id = id
        self.shipperId = shipperId
        self.shopId = shopId
        self.status = status
        self.codeBill = codeBill
        self.costShip = costShip
        self.note = note
        self.shipperName = shipperName
        self.phone = phone
        self.avartarLocation = avartarLocation
        self.likeNumber = likeNumber
        self.dislikeNumber = dislikeNumber
        self.pointNumber = pointNumber
    }
}