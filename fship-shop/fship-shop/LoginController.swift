//
//  ViewController.swift
//  fship-shop
//
//  Created by vhcsoft on 3/17/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class LoginController: UIViewController {

    @IBOutlet weak var txtUsername: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var checkDisplayPassword: SCheckBox!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        loadCheckboxDisplayPassword()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        let isLoggedIn:Int = prefs.integerForKey("ISLOGGEDIN") as Int
        
        if (isLoggedIn != 1) {
            
        } else {
            self.performSegueWithIdentifier("goto_home", sender: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func loadCheckboxDisplayPassword() {
        self.checkDisplayPassword.color(UIColor.grayColor(), forState: UIControlState.Normal)
        self.checkDisplayPassword.textLabel.font = UIFont(name: "Times New Roman", size: 14)
        self.checkDisplayPassword.textLabel.text = "Hiển thị mật khẩu"
        self.checkDisplayPassword.addTarget(self, action: "tapCheckDisplayPassword:", forControlEvents: UIControlEvents.ValueChanged)
    }
    
    func tapCheckDisplayPassword(checkBox: SCheckBox!){
        println("\(checkBox.checked)")
        if checkBox.checked {
            txtPassword.secureTextEntry = false
        }
        else{
            txtPassword.secureTextEntry = true
        }
    }

    @IBAction func signinTapped(sender: UIButton) {
        var username:NSString = txtUsername.text
        var password:NSString = txtPassword.text
        if ( username.isEqualToString("") || password.isEqualToString("") ) {
            SCLAlertView().showNotice(self, title: TITLE_ALERT_APP, subTitle: message_not_full_information, closeButtonTitle: title_button_ok, duration: 0.0)
        }
        else {
            SwiftLoader.show(title: title_loading, animated: true)
            
            let PasswordString = "\(username):\(password)"
            let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
            let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
            
            let urlPath: String = SERVER_URL + API_LOGIN_SHOPPER
            var url: NSURL = NSURL(string: urlPath)!
            
            var request: NSMutableURLRequest = NSMutableURLRequest(URL: url)
            request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
            request.HTTPMethod = "GET"
            
            //2 URL Request with AsynchronousRequest with json output
            NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler:{ (response: NSURLResponse!, data: NSData!, error: NSError!) -> Void in
                SwiftLoader.hide()
                if ( data != nil ) {
                    let res = response as NSHTTPURLResponse!;
                    //NSLog("Response code: %ld", res.statusCode);
                    if (res != nil)
                    {
                        var err: NSError
                        
                        var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
                        println("\(jsonResult)")
                        
                        let success:NSInteger = jsonResult.valueForKey("id") as NSInteger
                        let sign:NSString = jsonResult.valueForKey("sign") as NSString
                        NSLog("Success: %ld", success);
                        
                        if(success != 0)
                        {
                            NSLog("Login SUCCESS");
                            var prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
                            prefs.setObject(username, forKey: "USERNAME")
                            prefs.setObject(password, forKey: "PASSWORD")
                            prefs.setObject(sign, forKey: "SIGN")
                            prefs.setObject(success, forKey: "ID")
                            prefs.setInteger(1, forKey: "ISLOGGEDIN")
                            prefs.synchronize()
                            self.performSegueWithIdentifier("goto_home", sender: self)
                        }
                        else{
                            var error_msg:NSString
                            
                            if jsonResult["error_message"] as? NSString != nil {
                                error_msg = jsonResult["error_message"] as NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            SCLAlertView().showError(self, title: TITLE_ALERT_APP, subTitle: error_msg, closeButtonTitle: title_button_ok, duration: 0.0)
                        }
                    }
                    else{
                        SCLAlertView().showError(self, title: TITLE_ALERT_APP, subTitle: "Thông tin đăng nhập không đúng.", closeButtonTitle: title_button_ok, duration: 0.0)
                    }
                    
                }
                else{
                    SCLAlertView().showError(self, title: TITLE_ALERT_APP, subTitle: "Kết nối thất bại.", closeButtonTitle: title_button_ok, duration: 0.0)
                }
            })
        }
    }
    
    
    

}

