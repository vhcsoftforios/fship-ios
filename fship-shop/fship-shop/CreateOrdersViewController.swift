//
//  CreateOrdersViewController.swift
//  fship-shop
//
//  Created by vhcsoft on 3/17/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class CreateOrdersViewController: UIViewController, APIProvinceFromControllerProtocol, APIProvinceToControllerProtocol, APIDistrictFromControllerProtocol, APIDistrictToControllerProtocol, APISeverityControllerProtocol, UIPickerViewDelegate, UIToolbarDelegate, UITextViewDelegate {

    @IBOutlet weak var dropDownProvinceFrom: IQDropDownTextField!
    @IBOutlet weak var dropDownDistrictFrom: IQDropDownTextField!
    
    @IBOutlet weak var txtAddressFrom: UITextField!
    
    @IBOutlet weak var dropDownProvinceTo: IQDropDownTextField!
    @IBOutlet weak var dropDownDistrictTo: IQDropDownTextField!
    
    @IBOutlet weak var txtAddressTo: UITextField!
    @IBOutlet weak var txtCostProduct: UITextField!
    @IBOutlet weak var txtCostShipShop: UITextField!
    @IBOutlet weak var txtPhoneCustomer: UITextField!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var dropDownSeverity: IQDropDownTextField!
    
    @IBOutlet weak var txtDeadline: UITextField!
    @IBOutlet weak var txtNote: UITextView!
    
    var resultKeysProvinceFrom = [String]()
    var resultValuesProvinceFrom = [String]()
    var resultKeysDistrictFrom = [String]()
    var resultValuesDistrictFrom = [String]()
    
    var apiProvince = APIProvinceController()
    var apiDistrict = APIDistrictController()
    var apiSeverity = APISeverityController()
    
    var resultKeysProvinceTo = [String]()
    var resultValuesProvinceTo = [String]()
    var resultKeysDistrictTo = [String]()
    var resultValuesDistrictTo = [String]()
    
    var resultKeysSeverity = [String]()
    var resultValuesSeverity = [String]()
    
    var myToolBar: UIToolbar!
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Tạo đơn hàng"
        loadMenuButton(self, menuButton)
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        self.contentView.backgroundColor = UIColor(white: 0.95, alpha: 1.0)

        apiProvince.loadDataProvinceFrom()
        apiProvince.delegateFrom = self
        
        apiProvince.loadDataProvinceTo()
        apiProvince.delegateTo = self
        
        apiSeverity.loadDataSeverity()
        apiSeverity.delegate = self
        
        myToolBar = UIToolbar(frame: CGRectMake(0, self.view.frame.size.height/6, self.view.frame.size.width, 40.0))
        myToolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
        myToolBar.backgroundColor = UIColor.blackColor()
        myToolBar.barStyle = UIBarStyle.Black
        myToolBar.tintColor = UIColor.whiteColor()
        
        //ToolBar
        let myToolBarButton = UIBarButtonItem(title: "Close", style: .Bordered, target: self, action: "onClick:")
        myToolBarButton.tag = 1
        myToolBar.items = [myToolBarButton]
        
        loadDatePicker()
        loadTextView()
    }
    
    //================================= Text View
    func loadTextView(){
        txtNote.delegate = self
        if (txtNote.text == "") {
            textViewDidEndEditing(txtNote)
        }
        var tapDismiss = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        self.view.addGestureRecognizer(tapDismiss)
    }
    
    func dismissKeyboard(){
        txtNote.resignFirstResponder()
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if (textView.text == "") {
            textView.text = "Chi tiết đơn hàng..."
            textView.textColor = UIColor.lightGrayColor()
        }
        textView.resignFirstResponder()
    }
    
    func textViewDidBeginEditing(textView: UITextView){
        if (textView.text == "Chi tiết đơn hàng..."){
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
        textView.becomeFirstResponder()
    }
    //=================================
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    private func clearDataInput(){
        txtAddressFrom.text = ""
        txtAddressTo.text = ""
        txtCostProduct.text = ""
        txtCostShipShop.text = ""
        txtNote.text = ""
        txtPhoneCustomer.text = ""
    }
    
    func didReceiveAPIProvinceFromResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            //var arrayResult = [Province]()
            for element in resultsArr {
                let code = element["code"]! as? String
                let province = element["province"]! as? String
                
                self.resultKeysProvinceFrom.append(code!)
                self.resultValuesProvinceFrom.append(province!)
                
                //var record = Province(code: code?, province: province?)
                //arrayResult.append(record)
            }
            //self.arrayOfProvince = arrayResult
            
            self.dropDownProvinceFrom.setValue(self.resultValuesProvinceFrom, forKey: "itemList")
            self.apiDistrict.loadDataDistrictFromByKey(self.resultKeysProvinceFrom[0])
            self.apiDistrict.delegateFrom = self
        })
    }
    
    func didReceiveAPIProvinceToResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            //var arrayResult = [Province]()
            for element in resultsArr {
                let code = element["code"]! as? String
                let province = element["province"]! as? String
                
                self.resultKeysProvinceTo.append(code!)
                self.resultValuesProvinceTo.append(province!)
                
                //var record = Province(code: code?, province: province?)
                //arrayResult.append(record)
            }
            //self.arrayOfProvince = arrayResult
            
            self.dropDownProvinceTo.setValue(self.resultValuesProvinceTo, forKey: "itemList")
            self.apiDistrict.loadDataDistrictToByKey(self.resultKeysProvinceTo[0])
            self.apiDistrict.delegateTo = self
        })
    }
    
    func didReceiveAPIDistrictFromResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            //var arrayResult = [District]()
            self.resultKeysDistrictFrom   = [String]()
            self.resultValuesDistrictFrom   = [String]()
            
            for element in resultsArr {
                let district = element["district"]! as? String
                let districtName = element["districtName"]! as? String
                
                self.resultKeysDistrictFrom.append(district!)
                self.resultValuesDistrictFrom.append(districtName!)
                
                //var record = District(district: district?, districtName: districtName?)
                //arrayResult.append(record)
            }
            //self.arrayOfDistrict = arrayResult
            
            self.dropDownDistrictFrom.setValue(self.resultValuesDistrictFrom, forKey: "itemList")
        })
    }
    
    func didReceiveAPIDistrictToResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            //var arrayResult = [District]()
            self.resultKeysDistrictTo   = [String]()
            self.resultValuesDistrictTo   = [String]()
            
            for element in resultsArr {
                let district = element["district"]! as? String
                let districtName = element["districtName"]! as? String
                
                self.resultKeysDistrictTo.append(district!)
                self.resultValuesDistrictTo.append(districtName!)
                
                //var record = District(district: district?, districtName: districtName?)
                //arrayResult.append(record)
            }
            //self.arrayOfDistrict = arrayResult
            
            self.dropDownDistrictTo.setValue(self.resultValuesDistrictTo, forKey: "itemList")
        })
    }
    
    func didReceiveAPISeverityResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            //var arrayResult = [District]()
            
            for element in resultsArr {
                let name = element["name"]! as? String
                let value = element["value"]! as? String
                
                self.resultKeysSeverity.append(name!)
                self.resultValuesSeverity.append(value!)
                
                //var record = District(district: district?, districtName: districtName?)
                //arrayResult.append(record)
            }
            //self.arrayOfDistrict = arrayResult
            
            self.dropDownSeverity.setValue(self.resultValuesSeverity, forKey: "itemList")
        })
    }
    
    @IBAction func editingDidEndProvinceFrom(sender: AnyObject) {
        var index = dropDownProvinceFrom.selectedRow
        var province:NSString = dropDownProvinceFrom.text
        println("\(index) \(province)")
        if index >= 0 {
            self.apiDistrict.loadDataDistrictFromByKey(self.resultKeysProvinceFrom[index])
            self.apiDistrict.delegateFrom = self
        }
    }
    
    @IBAction func editingDidEndProvinceTo(sender: AnyObject) {
        var index = dropDownProvinceTo.selectedRow
        var province:NSString = dropDownProvinceTo.text
        println("\(index) \(province)")
        if index >= 0 {
            self.apiDistrict.loadDataDistrictToByKey(self.resultKeysProvinceTo[index])
            self.apiDistrict.delegateTo = self
        }
    }
    
    //Dong lai pickerView
    func onClick(sender: UIBarButtonItem) {
        /*txtProvinceFrom.resignFirstResponder()
        txtDistrictFrom.resignFirstResponder()
        txtProvinceTo.resignFirstResponder()
        txtDistrictTo.resignFirstResponder()
        txtSeverity.resignFirstResponder()*/
        txtDeadline.resignFirstResponder()
    }
    
    func loadDatePicker(){
        // DatePickerを生成する.
        var myDatePicker: UIDatePicker = UIDatePicker()
        
        // datePickerを設定（デフォルトでは位置は画面上部）する.
        myDatePicker.frame = CGRectMake(0, self.view.bounds.height/4, 0, 0)
        myDatePicker.timeZone = NSTimeZone.localTimeZone()
        myDatePicker.backgroundColor = UIColor.grayColor()
        myDatePicker.layer.cornerRadius = 5.0
        myDatePicker.layer.shadowOpacity = 0.5
        
        // 値が変わった際のイベントを登録する.
        myDatePicker.addTarget(self, action: "onDidChangeDate:", forControlEvents: .ValueChanged)
        
        // Today
        let myDateFormatter: NSDateFormatter = NSDateFormatter()
        myDateFormatter.dateFormat = "yyyy/MM/dd hh:mm"
        var mySelectedDate: NSString = myDateFormatter.stringFromDate(NSDate())
        txtDeadline.text = mySelectedDate
        //
        
        txtDeadline.inputView = myDatePicker
        txtDeadline.inputAccessoryView = myToolBar
    }
    
    /*
    DatePicker
    */
    func onDidChangeDate(sender: UIDatePicker){
        let myDateFormatter: NSDateFormatter = NSDateFormatter()
        myDateFormatter.dateFormat = "yyyy/MM/dd hh:mm"
        var mySelectedDate: NSString = myDateFormatter.stringFromDate(sender.date)
        txtDeadline.text = mySelectedDate
    }
    
    /*===========================================================================================*/
    @IBAction func btnGoiShip(sender: UIButton) {
        var provinceFrom:NSString = dropDownProvinceFrom.text
        var districtFrom:NSString = dropDownDistrictFrom.text
        var addressFrom:NSString = txtAddressFrom.text
        var districtTo:NSString = dropDownDistrictFrom.text
        var provinceTo:NSString = dropDownProvinceTo.text
        var addressTo:NSString =  txtAddressTo.text
        var costProduct:NSString = txtCostProduct.text
        var costShipShop:NSString = txtCostShipShop.text
        var phoneCustomer:NSString =  txtPhoneCustomer.text
        
        let myDateFormatter: NSDateFormatter = NSDateFormatter()
        myDateFormatter.dateFormat = "yyyy/MM/dd hh:mm"
        var mySelectedDate: NSDate = myDateFormatter.dateFromString(txtDeadline.text)!
        NSTimeInterval(mySelectedDate.timeIntervalSince1970)
        var deadline:NSString = String(Int64(mySelectedDate.timeIntervalSince1970)) //txtDeadline.text
        var severity:NSString = dropDownSeverity.text
        var note:NSString =  txtNote.text
        
        if ( provinceFrom.isEqualToString("")
            || districtFrom.isEqualToString("")
            || addressFrom.isEqualToString("")
            || provinceTo.isEqualToString("")
            || districtTo.isEqualToString("")
            || addressTo.isEqualToString("")
            || costProduct.isEqualToString("")
            || costShipShop.isEqualToString("")
            || phoneCustomer.isEqualToString("")
            || deadline.isEqualToString("")
            || severity.isEqualToString("")
            ) {
                SCLAlertView().showNotice(self, title: TITLE_ALERT_APP, subTitle: message_not_full_information, closeButtonTitle: title_button_ok, duration: 0.0)
        }
        else{
            SwiftLoader.show(title: title_loading, animated: true)
            requestCreateOrders(provinceFrom, districtFrom: districtFrom, addressFrom: addressFrom, provinceTo: provinceTo, districtTo: districtTo, addressTo: addressTo, costProduct: costProduct, costShipShop: costShipShop, severity: severity, deadline: deadline, note: note, phoneCustomer: phoneCustomer)
        }
    }
    
    func requestCreateOrders(provinceFrom: String, districtFrom: String, addressFrom: String, provinceTo: String, districtTo: String, addressTo: String, costProduct: String, costShipShop: String, severity: String, deadline: String, note: String, phoneCustomer: String){
        
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var username:NSString = prefs.valueForKey("USERNAME") as NSString
        var password:NSString = prefs.valueForKey("PASSWORD") as NSString
        var sign:NSString = prefs.valueForKey("SIGN") as NSString
        var shopId:NSInteger = prefs.valueForKey("ID") as NSInteger
        
        
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        
        let jsonString = "{\"sign\": \"\(sign)\",\"shopId\": \"\(shopId)\",\"provinceFrom\": \"\(provinceFrom)\",\"districtFrom\": \"\(districtFrom)\",\"addressFrom\": \"\(addressFrom)\",\"provinceTo\":\"\(provinceTo)\",\"districtTo\": \"\(districtTo)\",\"addressTo\": \"\(addressTo)\",\"costProduct\": \"\(costProduct)\",\"costShipShop\": \"\(costShipShop)\",\"severity\": \"\(severity)\",\"deadline\": \"\(deadline)\",\"note\": \"\(note)\",\"phoneCustomer\": \"\(phoneCustomer)\"}"
        
        let url = NSURL(string: SERVER_URL + API_CALL_ORDERS)
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        var err: NSError?
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttp)
        
    }
    
    func getHttp(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                println("response was not 200: \(res)")
                return
            }
            else{
                var myData:NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                println(myData)
                let alert = SCLAlertView()
                alert.addButton(title_button_menu_orders) {
                    /*var detailedViewController: OrdersViewController = self.storyboard?.instantiateViewControllerWithIdentifier("OrdersViewController") as OrdersViewController
                    let navigationController = UINavigationController(rootViewController: detailedViewController)
                    self.presentViewController(navigationController, animated: true, completion: nil)*/
                    
                    let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("OrdersViewController") as OrdersViewController
                    self.navigationController?.pushViewController(secondViewController, animated: true)
                }
                alert.addButton(title_button_ok) {
                    self.clearDataInput()
                }
                alert.showSuccess(self, title: TITLE_ALERT_APP, subTitle: "Tạo đơn hàng thành công. Bạn có muốn tạo thêm đơn hàng không?", closeButtonTitle: title_button_cancel, duration: 0.0)
            }
        }
        if (error != nil) {
            println("error submitting request: \(error)")
            return
        }
    }
    
}
