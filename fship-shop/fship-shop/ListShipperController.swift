//
//  ListShipperControllerViewController.swift
//  fship-shop
//
//  Created by vhcsoft on 3/20/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class ListShipperController: UIViewController, UITableViewDataSource, UITableViewDelegate, APIListShipperControllerProtocol {

    @IBOutlet weak var barButtonBack: UIBarButtonItem!
    @IBOutlet weak var barButtonRefresh: UIBarButtonItem!
    @IBOutlet weak var lblCodeBill: UILabel!
    var codeBill: String?
    var id: String?
    
    @IBOutlet weak var listShipperTableView: UITableView!
    var arrayOfListShipper: [Shipper] = [Shipper]()
    var apiListShipper = APIListShipperController()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Danh sách Shipper nhận"
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        
        barButtonBack.target = self
        barButtonBack.action = "clickBackLogin:"
        
        barButtonRefresh.target = self
        barButtonRefresh.action = "clickButtonRefresh:"
        
        var nib = UINib(nibName: "ListShipperCell", bundle: nil)
        listShipperTableView.registerNib(nib, forCellReuseIdentifier: "tvcListShipperCell")
        
        apiListShipper.loadDataListShipper(codeBill!)
        apiListShipper.delegate = self
        
        listShipperTableView.delegate = self
        listShipperTableView.dataSource = self
        lblCodeBill.text = codeBill
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func clickBackLogin(sender: UIBarButtonItem!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func clickButtonRefresh(sender: UIBarButtonItem!) {
        loadRefreshListShipper()
    }
    
    func loadRefreshListShipper(){
        var nib = UINib(nibName: "ListShipperCell", bundle: nil)
        listShipperTableView.registerNib(nib, forCellReuseIdentifier: "tvcListShipperCell")
        
        self.apiListShipper.loadDataListShipper(codeBill!)
        self.apiListShipper.delegate = self
        self.listShipperTableView!.reloadData()
        
    }
    
    //===========================================================================
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfListShipper.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: ListShipperCell = tableView.dequeueReusableCellWithIdentifier("tvcListShipperCell") as ListShipperCell
        
        var record: Shipper = arrayOfListShipper[indexPath.row]
            cell.setListShipperCell(record)
        
        // Click vao image tich chon shipper
        cell.imageChecked?.userInteractionEnabled = true
        cell.imageChecked?.tag = indexPath.row
        var tapped:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: "TappedOnImageChecked:")
        tapped.numberOfTapsRequired = 1
        cell.imageChecked?.addGestureRecognizer(tapped)
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.listShipperTableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 110.0
    }
    
    func didReceiveAPIListShipperResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            var arrayResult = [Shipper]()
            for element in resultsArr {
                let id = element["id"]! as Int
                let shipperId = element["shipperId"]! as? Int
                let shopId = element["shopId"]! as? Int
                let status = element["status"]! as? Int
                let codeBill = element["codeBill"]! as? String
                let costShip = element["costShip"]! as? String
                let note = element["note"]! as? String
                let shipperName = element["shipperName"]! as? String
                let phone = element["phone"]! as? String
                let avartarLocation = element["avartarLocation"]! as? String
                let likeNumber = element["likeNumber"]! as? Int
                let dislikeNumber = element["dislikeNumber"] as? Int
                let pointNumber = element["pointNumber"]! as? Int
                
                var record = Shipper(id: id, shipperId: shipperId?, shopId: shopId?, status: status?, codeBill: codeBill?, costShip: costShip?, note: note?, shipperName: shipperName?, phone: phone?, avartarLocation: avartarLocation?, likeNumber: likeNumber?, dislikeNumber: dislikeNumber?, pointNumber: pointNumber?)
                
                arrayResult.append(record)
            }
            self.arrayOfListShipper = arrayResult
            
            self.listShipperTableView!.reloadData()
        })
    }
    
    var index = Int?()
    //=====================================================================================
    func TappedOnImageChecked(sender:UITapGestureRecognizer){
        println(sender.view?.tag)
        index = sender.view?.tag
        self.id = String(arrayOfListShipper[index!].id)
        
        let alert = SCLAlertView()
        alert.addButton(title_button_ok) {
            self.setAcceptShipper(self.id!)
        }
        alert.showWarning(self, title: TITLE_ALERT_APP, subTitle: "Bạn có chắc chắn muốn chọn Shipper này không?", closeButtonTitle: title_button_cancel, duration: 0.0)
        
    }
    
    func setAcceptShipper(id: String){
        
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var username:NSString = prefs.valueForKey("USERNAME") as NSString
        var password:NSString = prefs.valueForKey("PASSWORD") as NSString
        
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
    
        let url = NSURL(string: SERVER_URL + API_UPDATE_STATUS.replace("{0}", withString: "2").replace("{1}", withString: id))
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        var err: NSError?
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttp)
        
    }
    
    func getHttp(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                println("response was not 200: \(res)")
                return
            }
            else{
                var myData:NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                println(myData)
                
                SCLAlertView().showSuccess(self, title: TITLE_ALERT_APP, subTitle: "Lựa chọn Shipper thành công.", closeButtonTitle: title_button_ok, duration: 0.0)
                
                // Ve menu quan ly don hang
                let secondViewController = self.storyboard?.instantiateViewControllerWithIdentifier("OrdersViewController") as OrdersViewController
                self.navigationController?.pushViewController(secondViewController, animated: true)
            }
        }
        if (error != nil) {
            println("error submitting request: \(error)")
            return
        }
        
    }
}
