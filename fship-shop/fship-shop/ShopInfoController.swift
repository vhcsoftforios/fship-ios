//
//  ShopInfoController.swift
//  fship-shop
//
//  Created by vhcsoft on 3/19/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class ShopInfoController: UIViewController, APIProvinceControllerProtocol, APIDistrictControllerProtocol, UITextViewDelegate {

    @IBOutlet weak var imageAvatar: UIImageView!
    @IBOutlet weak var txtNote: UITextView!
    @IBOutlet weak var txtUsername: UILabel!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var txtPhoneNumber: UITextField!
    //@IBOutlet weak var txtProvince: UITextField!
    //@IBOutlet weak var txtDistrict: UITextField!
    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var lblLikeNumber: UILabel!
    @IBOutlet weak var lblDislikeNumber: UILabel!
    @IBOutlet weak var lblPointNumber: UILabel!
    var id = Int()
    
    @IBOutlet weak var dropDownProvince: IQDropDownTextField!
    @IBOutlet weak var dropDownDistrict: IQDropDownTextField!
    var apiProvince = APIProvinceController()
    var apiDistrict = APIDistrictController()
    
    var resultKeysProvince = [String]()
    var resultValuesProvince = [String]()
    var resultKeysDistrict = [String]()
    var resultValuesDistrict = [String]()
    
    @IBOutlet weak var contentView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Thông tin Shop"
        loadMenuButton(self, menuButton)
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        self.contentView.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        apiProvince.loadDataProvince()
        apiProvince.delegate = self
        
        loadTextView()
        getAPIshopInfo()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //================================= Text View
    func loadTextView(){
        txtNote.delegate = self
        if (txtNote.text == "") {
            textViewDidEndEditing(txtNote)
        }
        var tapDismiss = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        self.view.addGestureRecognizer(tapDismiss)
    }
    
    func dismissKeyboard(){
        txtNote.resignFirstResponder()
    }
    
    func textViewDidEndEditing(textView: UITextView) {
        if (textView.text == "") {
            textView.text = "Giới thiệu về shop..."
            textView.textColor = UIColor.lightGrayColor()
        }
        textView.resignFirstResponder()
    }
    
    func textViewDidBeginEditing(textView: UITextView){
        if (textView.text == "Giới thiệu về shop..."){
            textView.text = ""
            textView.textColor = UIColor.blackColor()
        }
        textView.becomeFirstResponder()
    }
    
    func didReceiveAPIProvinceResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            //var arrayResult = [Province]()
            for element in resultsArr {
                let code = element["code"]! as? String
                let province = element["province"]! as? String
                
                self.resultKeysProvince.append(code!)
                self.resultValuesProvince.append(province!)
                
                //var record = Province(code: code?, province: province?)
                //arrayResult.append(record)
            }
            //self.arrayOfProvince = arrayResult
            
            self.dropDownProvince.setValue(self.resultValuesProvince, forKey: "itemList")
            self.apiDistrict.loadDataDistrictByKey(self.resultKeysProvince[0])
            self.apiDistrict.delegate = self
        })
    }
    
    func didReceiveAPIDistrictResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            //var arrayResult = [District]()
            self.resultKeysDistrict   = [String]()
            self.resultValuesDistrict   = [String]()
            
            for element in resultsArr {
                let district = element["district"]! as? String
                let districtName = element["districtName"]! as? String
                
                self.resultKeysDistrict.append(district!)
                self.resultValuesDistrict.append(districtName!)
                
                //var record = District(district: district?, districtName: districtName?)
                //arrayResult.append(record)
            }
            //self.arrayOfDistrict = arrayResult
            
            self.dropDownDistrict.setValue(self.resultValuesDistrict, forKey: "itemList")
        })
    }
    
    @IBAction func editingDidEndProvince(sender: AnyObject) {
        
        var index = dropDownProvince.selectedRow
        var province:NSString = dropDownProvince.text
        println("\(index) \(province)")
        if index >= 0 {
            self.apiDistrict.loadDataDistrictByKey(self.resultKeysProvince[index])
            self.apiDistrict.delegate = self
        }
    }
    
    //=================================
    
    @IBAction func btnUpdateTapped(sender: UIButton) {
        var phone:NSString = txtPhoneNumber.text
        var province:NSString = dropDownProvince.text
        var district:NSString = dropDownDistrict.text
        var address:NSString = txtAddress.text
        var note:NSString =  txtNote.text
        
        if ( phone.isEqualToString("")
            || province.isEqualToString("")
            || district.isEqualToString("")
            || address.isEqualToString("")
            ) {
                SCLAlertView().showNotice(self, title: TITLE_ALERT_APP, subTitle: message_not_full_information, closeButtonTitle: title_button_ok, duration: 0.0)
        }
        else{
            SwiftLoader.show(title: title_loading, animated: true)
            let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
            var username:NSString = prefs.valueForKey("USERNAME") as NSString
            postUpdateShopInfo(id, username: username, phone: phone, province: province, district: district, address: address, avartarLocation: "", note: note)
        }
    }
    
    //=================================
    func getAPIshopInfo() -> Void {
        SwiftLoader.show(title: title_loading, animated: true)
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var username:NSString = prefs.valueForKey("USERNAME") as NSString
        var password:NSString = prefs.valueForKey("PASSWORD") as NSString
        
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        
        let url = NSURL(string: SERVER_URL + API_SHOP_INFO.replace("{0}", withString: username))
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "GET"
        var err: NSError?
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttpShopInfo)
        
    }
    
    func getHttpShopInfo(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                println("response was not 200: \(res)")
                return
            }
            else{
                var myData:NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                println(myData)
                
                var parseError: NSError?
                
                var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
                println("jsonResult: \(jsonResult)")
                
                id = jsonResult["id"]! as Int
                let username = jsonResult["username"]! as? String
                let phone = jsonResult["phone"]! as? String
                let province = jsonResult["province"]! as? String
                let district = jsonResult["district"]! as? String
                let address = jsonResult["address"]! as? String
                let avartarLocation = jsonResult["avartarLocation"]! as? String
                var likeNumber = jsonResult["likeNumber"]! as Int
                var dislikeNumber = jsonResult["dislikeNumber"]! as Int
                var pointNumber = jsonResult["pointNumber"]! as Int
                let note = jsonResult["note"]! as? String
                let facebookId = jsonResult["facebookId"]! as? String
                let sign = jsonResult["sign"]! as? String
                
                // Avatar
                let url = NSURL(string: avartarLocation!)
                let data = NSData(contentsOfURL: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check
                self.imageAvatar.image = UIImage(data: data!)
                //
                txtUsername.text = username
                txtNote.text = note
                txtPhoneNumber.text = phone
                dropDownProvince.selectedItem = province
                dropDownDistrict.selectedItem = district
                txtAddress.text = address
                lblLikeNumber.text = String(likeNumber)
                lblDislikeNumber.text = String(dislikeNumber)
                lblPointNumber.text = String(pointNumber)
                
                return
            }
        }
        if (error != nil) {
            println("error submitting request: \(error)")
            return
        }
    }
    
    //=========================================
    func postUpdateShopInfo(id: Int, username: String, phone: String, province: String, district: String, address: String, avartarLocation: String, note: String){
        
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var username:NSString = prefs.valueForKey("USERNAME") as NSString
        var password:NSString = prefs.valueForKey("PASSWORD") as NSString
        
        
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        
        let jsonString = "{\"id\": \"\(id)\",\"username\": \"\(username)\",\"phone\": \"\(phone)\",\"province\": \"\(province)\",\"district\": \"\(district)\",\"address\":\"\(address)\",\"avartarLocation\": \"\(avartarLocation)\",\"note\": \"\(note)\"}"
        
        let url = NSURL(string: SERVER_URL + API_UPDATE_SHOP_INFO)
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "POST"
        var err: NSError?
        request.HTTPBody = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttpUpdate)
        
    }
    
    func getHttpUpdate(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            if httpResponse.statusCode != 200 {
                println("response was not 200: \(res)")
                return
            }
            else{
                var myData:NSString = NSString(data: data!, encoding: NSUTF8StringEncoding)!
                println(myData)
                SCLAlertView().showSuccess(self, title: TITLE_ALERT_APP, subTitle: "Cập nhật thông tin thành công.", closeButtonTitle: title_button_ok, duration: 0.0)
                return
            }
        }
        if (error != nil) {
            println("error submitting request: \(error)")
            return
        }
        
        SCLAlertView().showError(self, title: TITLE_ALERT_APP, subTitle: "Cập nhật thông tin thất bại.", closeButtonTitle: title_button_ok, duration: 0.0)
    }
    
}
