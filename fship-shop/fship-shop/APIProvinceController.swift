//
//  APIProvinceController.swift
//  fship-shop
//
//  Created by vhcsoft on 4/2/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import Foundation

protocol APIProvinceControllerProtocol {
    func didReceiveAPIProvinceResults(results: NSArray)
}

protocol APIProvinceFromControllerProtocol {
    func didReceiveAPIProvinceFromResults(results: NSArray)
}

protocol APIProvinceToControllerProtocol {
    func didReceiveAPIProvinceToResults(results: NSArray)
}

class APIProvinceController {
    
    var delegate: APIProvinceControllerProtocol?
    var delegateFrom: APIProvinceFromControllerProtocol?
    var delegateTo: APIProvinceToControllerProtocol?
    
    init() {
    }
    
    func loadDataProvince() -> Void {
        var request = HTTPTask()
        request.GET(SERVER_URL + API_PROVINCE, parameters: nil, success: {(response: HTTPResponse) in
            if let data = response.responseObject as? NSData {
                var parseError: NSError?
                let responseObject = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &parseError) as NSArray
                
                self.delegate?.didReceiveAPIProvinceResults(responseObject)
                //println(responseObject)
            }
            },failure: {(error: NSError, response: HTTPResponse?) in
                println("error: \(error)")
        })
    }
    
    func loadDataProvinceFrom() -> Void {
        var request = HTTPTask()
        request.GET(SERVER_URL + API_PROVINCE, parameters: nil, success: {(response: HTTPResponse) in
            if let data = response.responseObject as? NSData {
                var parseError: NSError?
                let responseObject = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &parseError) as NSArray
                
                self.delegateFrom?.didReceiveAPIProvinceFromResults(responseObject)
                //println(responseObject)
            }
            },failure: {(error: NSError, response: HTTPResponse?) in
                println("error: \(error)")
        })
    }
    
    func loadDataProvinceTo() -> Void {
        var request = HTTPTask()
        request.GET(SERVER_URL + API_PROVINCE, parameters: nil, success: {(response: HTTPResponse) in
            if let data = response.responseObject as? NSData {
                var parseError: NSError?
                let responseObject = NSJSONSerialization.JSONObjectWithData(data, options: nil, error: &parseError) as NSArray
                
                self.delegateTo?.didReceiveAPIProvinceToResults(responseObject)
                //println(responseObject)
            }
            },failure: {(error: NSError, response: HTTPResponse?) in
                println("error: \(error)")
        })
    }
    
}