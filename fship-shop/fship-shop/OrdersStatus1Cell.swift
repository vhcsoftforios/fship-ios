//
//  OrdersStatus1Cell.swift
//  fship-shop
//
//  Created by vhcsoft on 3/23/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit
/*
Function: Quan ly don hang trang thai cho ship
Description:
Create Date: 23/03/2015
Create By: QuangBui
*/
class OrdersStatus1Cell: UITableViewCell {

    @IBOutlet weak var lblCodeBill: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblShipperName: UILabel!
    @IBOutlet weak var imageConfirm: UIImageView!
    @IBOutlet weak var imageDelete: UIImageView!
    @IBOutlet weak var imageCall: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setStatus1Cell(record: SysBill)
    {
        self.lblCodeBill.text = record.codeBill
        self.lblDistance.text = "( \(record.distance) Km)"
        self.lblShipperName.text = record.shipperName
    }
}
