//
//  NotifyViewController.swift
//  fship-shop
//
//  Created by vhcsoft on 3/18/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class NotifyViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, APINotificationControllerProtocol, UISearchBarDelegate {
    var popViewController : PopUpViewNotify!
    @IBOutlet weak var notificationTableView: UITableView!
    @IBOutlet weak var notificationSearchBar: UISearchBar!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    var searchActive : Bool = false
    var arrayOfNotification: [Notification] = [Notification]()
    var filtered:[Notification] = [Notification]()
    var apiNotification = APINotificationController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Danh sách thông báo"
        loadMenuButton(self, menuButton)
        
        self.view.backgroundColor = UIColor(white: 0.95, alpha: 1.0)
        
        apiNotification.loadDataNotification()
        apiNotification.delegate = self
        
        var nib = UINib(nibName: "vwNotificationCell", bundle: nil)
        notificationTableView.registerNib(nib, forCellReuseIdentifier: "tvcNotificationCell")
        
        notificationSearchBar.placeholder = "Tìm kiếm"
        notificationSearchBar.delegate = self
        notificationTableView.delegate = self
        notificationTableView.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(searchActive) {
            return filtered.count
        }
        return arrayOfNotification.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell: NotificationCell = tableView.dequeueReusableCellWithIdentifier("tvcNotificationCell") as NotificationCell
        
        var record: Notification
        if(searchActive){
            record = filtered[indexPath.row]
            cell.setNotificationCell(record)
        } else {
            record = arrayOfNotification[indexPath.row]
            cell.setNotificationCell(record)
        }
        
        if(record.messageType == tev_message_type_cancel || record.messageType == tev_message_type_delete)
        {
            cell.lblMessage?.textColor = UIColor.redColor()
        }
        if(record.messageType == tev_message_type_send)
        {
            cell.lblMessage?.textColor = UIColor.blueColor()
        }
        cell.lblMessage?.numberOfLines = 0;
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let record = arrayOfNotification[indexPath.row]
        /*var detailedViewController: DetailNotificationViewController = self.storyboard?.instantiateViewControllerWithIdentifier("DetailNotificationViewController") as DetailNotificationViewController
        detailedViewController.codeBill = record.codeBill
        
        let navigationController = UINavigationController(rootViewController: detailedViewController)
        self.presentViewController(navigationController, animated: true, completion: nil)*/
        self.popViewController = PopUpViewNotify(nibName: "popupNotify", bundle: nil)
        self.popViewController.showInView(self.view, withCodeBill: record.codeBill, animated: true)
        
        self.notificationTableView.deselectRowAtIndexPath(indexPath, animated: true)
        
    }
    
    func didReceiveAPINotificationResults(resultsArr: NSArray) {
        dispatch_async(dispatch_get_main_queue(), {
            var arrayResult = [Notification]()
            for element in resultsArr {
                let id = element["id"]! as Int
                let codeBill = element["codeBill"]! as String
                let userFrom = element["userFrom"]! as String
                let userTo = element["userTo"]! as String
                let messageType = element["messageType"]! as String
                let message = element["message"]! as String
                var record = Notification(id: id, codeBill: codeBill, userFrom: userFrom, userTo: userTo, messageType: messageType, message: message)
                arrayResult.append(record)
            }
            self.arrayOfNotification = arrayResult
            
            self.notificationTableView!.reloadData()
        })
    }
    
    /*===================================================================================*/
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = arrayOfNotification.filter({ (text) -> Bool in
            let tmp: NSString = text.message
            let range = tmp.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
            return range.location != NSNotFound
        })
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.notificationTableView.reloadData()
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
}
