//
//  SysBill.swift
//  fship-shop
//
//  Created by vhcsoft on 3/13/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import Foundation

class SysBill {
    
    var id: Int
    var codeBill: String?
    var costProduct: String?
    var severity: String?
    var shopId: Int?
    var shipperId: Int?
    var provinceFrom: String?
    var districtFrom: String?
    var addressFrom: String?
    var provinceTo: String?
    var districtTo: String?
    var addressTo: String?
    var note: String?
    var distance: Double
    var costShipShop: String?
    var costShipShipper: String?
    var status: Int
    var deadline: Int?
    var phoneCustomer: String?
    var latitude: String?
    var longtitude: String?
    var countShipper: Int
    var shipperName: String?
    var sign: String?
    var shopName: String?
    var checkLikeShip: Int
    var checkLikeShop: Int
    
    init(id: Int, codeBill: String, costProduct: String?, severity: String,
        shopId: Int?, shipperId: Int?, provinceFrom: String?, districtFrom: String?, addressFrom: String?, provinceTo: String?, districtTo: String?, addressTo: String?, note: String?, distance: Double, costShipShop: String?, costShipShipper: String?, status: Int, deadline: Int?, phoneCustomer: String?, latitude: String?, longtitude: String?, countShipper: Int, shipperName: String?, sign: String?, shopName: String?, checkLikeShip: Int, checkLikeShop: Int
        ) {
        self.id = id
        self.codeBill = codeBill
        self.costProduct = costProduct
        self.severity = severity
        self.shopId = shopId
        self.shipperId = shipperId
        self.provinceFrom = provinceFrom
        self.districtFrom = districtFrom
        self.addressFrom = addressFrom
        self.provinceTo = provinceTo
        self.districtTo = districtTo
        self.addressTo = addressTo
        self.note = note
        self.distance = distance
        self.costShipShop = costShipShop
        self.costShipShipper = costShipShipper
        self.status = status
        self.deadline = deadline
        self.phoneCustomer = phoneCustomer
        self.latitude = latitude
        self.longtitude = longtitude
        self.countShipper = countShipper
        self.shipperName = shipperName
        self.sign = sign
        self.shopName = shopName
        self.checkLikeShip = checkLikeShip
        self.checkLikeShop = checkLikeShop
    }
}