//
//  Province.swift
//  fship-shop
//
//  Created by vhcsoft on 4/2/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import Foundation

class Province {
    var code: String?
    var province: String?
    
    init(code: String?, province: String?
        ) {
            self.code = code
            self.province = province
    }
}