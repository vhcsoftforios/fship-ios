//
//  PopUpViewNotify.swift
//  fship-shop
//
//  Created by vhcsoft on 3/25/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit

class PopUpViewNotify: UIViewController {

    @IBOutlet weak var popUpView: UIView!
    @IBOutlet weak var lblCodeBill: UILabel!
    @IBOutlet weak var lblCostProduct: UILabel!
    @IBOutlet weak var lblCostShipShipper: UILabel!
    @IBOutlet weak var lblShopName: UILabel!
    @IBOutlet weak var lblShipperName: UILabel!
    @IBOutlet weak var lblPhoneCustomer: UILabel!
    @IBOutlet weak var lblAddressFrom: UILabel!
    @IBOutlet weak var lblAddressTo: UILabel!
    @IBOutlet weak var lblNote: UILabel!
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override init(nibName nibNameOrNil: String!, bundle nibBundleOrNil: NSBundle!) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.6)
        self.popUpView.layer.cornerRadius = 5
        self.popUpView.layer.shadowOpacity = 0.8
        self.popUpView.layer.shadowOffset = CGSizeMake(0.0, 0.0)
    }
    
    func showInView(
        aView: UIView!,
        withCodeBill codeBill : String!,
        animated: Bool)
    {
        aView.addSubview(self.view)
        getDetailOrdersByCodeBill(codeBill!)
        if animated
        {
            self.showAnimate()
        }
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
        self.view.alpha = 0.0;
        UIView.animateWithDuration(0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransformMakeScale(1.0, 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animateWithDuration(0.25, animations: {
            self.view.transform = CGAffineTransformMakeScale(1.3, 1.3)
            self.view.alpha = 0.0;
            }, completion:{(finished : Bool)  in
                if (finished)
                {
                    self.view.removeFromSuperview()
                }
        });
    }
    
    @IBAction func closePopup(sender: AnyObject) {
        self.removeAnimate()
    }
    
    //====================================================================
    func getDetailOrdersByCodeBill(codeBill: String) -> Void {
        SwiftLoader.show(title: title_loading, animated: true)
        let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var username:NSString = prefs.valueForKey("USERNAME") as NSString
        var password:NSString = prefs.valueForKey("PASSWORD") as NSString
        
        let PasswordString = "\(username):\(password)"
        let PasswordData = PasswordString.dataUsingEncoding(NSUTF8StringEncoding)
        let base64EncodedCredential = PasswordData!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.Encoding64CharacterLineLength)
        
        let url = NSURL(string: SERVER_URL + API_GET_BILL_INFO.replace("{0}", withString: codeBill))
        let request = NSMutableURLRequest(URL: url!)
        request.setValue("Basic \(base64EncodedCredential)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.HTTPMethod = "GET"
        var err: NSError?
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: self.getHttp)
    }
    
    func getHttp(res:NSURLResponse?,data:NSData?,error:NSError?){
        SwiftLoader.hide()
        if let httpResponse = res as? NSHTTPURLResponse {
            var parseError: NSError?
            
            var jsonResult: NSDictionary = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions.MutableContainers, error: nil) as NSDictionary
            println("jsonResult: \(jsonResult)")
            
            
            //let distance:NSInteger = jsonResult.valueForKey("distance") as NSInteger
            let distance = jsonResult["distance"]! as? NSString
            //let codeBill:NSString = jsonResult.valueForKey("codeBill") as NSString
            let codeBill = jsonResult["codeBill"]! as? NSString
            lblCodeBill.text = "\(codeBill) (\(distance) Km)"
            self.setValueLabel(lblCostProduct, jsonResult: jsonResult, valueKey: "costProduct")
            self.setValueLabel(lblCostShipShipper, jsonResult: jsonResult, valueKey: "costShipShipper")
            self.setValueLabel(lblShipperName, jsonResult: jsonResult, valueKey: "shipperName")
            self.setValueLabel(lblShopName, jsonResult: jsonResult, valueKey: "shopName")
            self.setValueLabel(lblPhoneCustomer, jsonResult: jsonResult, valueKey: "phoneCustomer")
            self.setValueLabel(lblAddressFrom, jsonResult: jsonResult, valueKey: "addressFrom")
            self.setValueLabel(lblAddressTo, jsonResult: jsonResult, valueKey: "addressTo")
            self.setValueLabel(lblNote, jsonResult: jsonResult, valueKey: "note")
        }
        if (error != nil) {
            println("error request: \(error)")
            return
        }
    }
    
    func setValueLabel(label: UILabel!, jsonResult: NSDictionary, valueKey: String)
    {
        if let latestValue = jsonResult[valueKey] as? String {
            label.text = latestValue
        }
        else{
            label.text = ""
        }
    }
    
}
