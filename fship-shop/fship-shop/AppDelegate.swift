//
//  AppDelegate.swift
//  fship-shop
//
//  Created by vhcsoft on 3/17/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit
import QuartzCore

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        self.setColorHeader(application)
        
        // Push Alert
        var types: UIUserNotificationType = UIUserNotificationType.Badge |
            UIUserNotificationType.Alert |
            UIUserNotificationType.Sound
        
        var settings: UIUserNotificationSettings = UIUserNotificationSettings( forTypes: types, categories: nil )
        
        application.registerUserNotificationSettings( settings )
        application.registerForRemoteNotifications()
        
        return true
    }
    
    func setColorHeader(application: UIApplication){
        // style the navigation bar
        let navColor = UIColor(red: 0.175, green: 0.458, blue: 0.831, alpha: 1.0)
        UINavigationBar.appearance().barTintColor = navColor
        UINavigationBar.appearance().tintColor = UIColor.whiteColor()
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.whiteColor()]
        
        // make the status bar white
        application.statusBarStyle = .LightContent
    }
    
    func application(application: UIApplication!, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData!) {
        
        
        // <>と" "(空白)を取る
        var characterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
        var deviceTokenString: String = ( deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet( characterSet )
            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
        println( deviceTokenString )
        //================================
        // setup registration
        let registration = AGDeviceRegistration(serverURL: NSURL(string: "http://14.160.91.174:9002/ag-push/")!)
        
        // attemp to register
        registration.registerWithClientInfo({ (clientInfo: AGClientDeviceInformation!) in
            // setup configuration
            clientInfo.deviceToken = deviceToken
            clientInfo.variantID = "e8149615-43e0-4d01-9c9d-7249947ddd80"
            clientInfo.variantSecret = "a2210ee1-afd8-4f89-9eb6-3e10a81e7ccd"
            
            let prefs:NSUserDefaults = NSUserDefaults.standardUserDefaults()
            let username:NSString? = prefs.valueForKey("USERNAME") as? NSString
            if username != nil{
                clientInfo.alias = "shop@" + username!
            }
            
            // apply the token, to identify THIS device
            let currentDevice = UIDevice()
            
            // --optional config--
            // set some 'useful' hardware information params
            clientInfo.operatingSystem = currentDevice.systemName
            clientInfo.osVersion = currentDevice.systemVersion
            clientInfo.deviceType = currentDevice.model
            },
            
            success: {
                println("UnifiedPush Server registration succeeded")
            },
            failure: {(error: NSError!) in
                println("failed to register, error: \(error.description)")
        })
    }

    // Loi khi khong gui device token
    func application( application: UIApplication!, didFailToRegisterForRemoteNotificationsWithError error: NSError! ) {
        println( error.localizedDescription )
        
    }
    
    // Nhan push tu server
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject], fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        println("New remote notification")
        /*var notification:NSDictionary = userInfo as NSDictionary
        
        var localNotification:UILocalNotification = UILocalNotification()
        localNotification.userInfo = userInfo
        localNotification.soundName = UILocalNotificationDefaultSoundName
        
        localNotification.alertAction = "Testing notifications on iOS8"
        localNotification.alertBody = "Woww it works!!"
        
        localNotification.fireDate = NSDate(timeIntervalSinceNow: 30)
        localNotification.category = "INVITE_CATEGORY";
        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)*/
        
        application.applicationIconBadgeNumber = 0
    
    }
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: NSDictionary) {
        var notification:NSDictionary = userInfo.objectForKey("aps") as NSDictionary
        if (notification.objectForKey("content-available") != nil){
            if (notification.objectForKey("content-available")?.isEqualToNumber(1) != nil)
            {
                NSNotificationCenter.defaultCenter().postNotificationName("reloadTimeline", object: nil)
            }
        }
        
    }
    
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

