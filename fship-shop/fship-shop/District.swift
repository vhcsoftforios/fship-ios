//
//  District.swift
//  fship-shop
//
//  Created by vhcsoft on 4/2/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import Foundation

class District {
    var district: String?
    var districtName: String?
    
    init(district: String?, districtName: String?
        ) {
            self.district = district
            self.districtName = districtName
    }
}