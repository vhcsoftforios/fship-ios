//
//  Contants.swift
//  fship-shop
//
//  Created by vhcsoft on 3/17/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import Foundation

//public let SERVER_URL                   = "http://14.160.91.174:8080/"
public let SERVER_URL                   = "http://fship.vhc.com.vn/"
//public let SERVER_URL                 = "http://192.168.1.226:8080"
public let SERVER_AERO_GEAR_URL         = "http://14.160.91.174:9002/ag-push/"

public let API_LOGIN_SHOPPER            = "api/user/auth/0"
public var API_CHANGE_PASSWORD_SHOP     = "api/user/changePassword/0";

public let API_SIGUP_SHOPPER            = "api/shop/add"
public let API_SHOP_INFO                = "api/shop/info/{0}";
public let API_UPDATE_SHOP_INFO         = "api/shop/info";
public let API_NOTIFICATION             = "api/notify"
public let API_CHANGE_AVARTAR           = "api/shop/change-avatar";

public let API_DISTRICT                 = "api/config/district"
public let API_PROVINCE                 = "api/config/province"
public let API_SEVERITY_CONFIG          = "api/config/severity"
public let API_CALL_ORDERS              = "api/bill/add"
public var API_GET_BILL_INFO            = "api/bill/info/{0}";
public var API_ORDERS_STATUS            = "api/bill/shop/{0}"
public var API_DELETE_ORDER             = "api/bill/del/{0}";
public let API_UPDATE_STATUS_ORDER      = "api/bill/update";
public var API_RECALL_SHIPPER           = "api/bill/call/{0}";

public var API_SHIPPER_LIST             = "api/req/{0}";
public var API_UPDATE_STATUS            = "api/req/{0}/{1}";

public var API_GET_COMMENT_INFO         = "api/assess/{0}";
public let API_UPDATE_COMMENT_INFO      = "api/assess/add";
public let API_UPDATE_LIKE_INFO         = "api/assess/rate";
public var API_GET_SHIPER_INFO          = "api/shipper/info/{0}";

public let tev_message_type_send        = "GUI YEU CAU"
public let tev_message_type_cancel      = "HUY YEU CAU"
public let tev_message_type_delete      = "XOA DON HANG"
public let tev_severity_value_normal    = "Bình thường"
public let tev_severity_value_fast      = "Chuyển nhanh"
public let message_not_full_information = "Vui lòng nhập đầy đủ thông tin."
public let TITLE_ALERT_APP              = "F-Ship"
public let TITLE_SIGUP_PAGE             = "Đăng ký thành viên"
public let title_button_ok              = "Đồng ý"
public let title_button_cancel          = "Huỷ bỏ"
public let title_button_new             = "Tạo mới"
public let title_button_menu_orders     = "Về menu đơn hàng"
public let title_loading                = "Đang tải..."