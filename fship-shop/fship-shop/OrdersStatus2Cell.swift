//
//  OrdersStatus2Cell.swift
//  fship-shop
//
//  Created by vhcsoft on 3/23/15.
//  Copyright (c) 2015 vhcsoft. All rights reserved.
//

import UIKit
/*
Function: Quan ly don hang trang thai dang ship
Description:
Create Date: 23/03/2015
Create By: QuangBui
*/
class OrdersStatus2Cell: UITableViewCell {

    @IBOutlet weak var lblCodeBill: UILabel!
    @IBOutlet weak var lblDistance: UILabel!
    @IBOutlet weak var lblShipperName: UILabel!
    @IBOutlet weak var imageReturns: UIImageView!
    @IBOutlet weak var imageConfirm: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setStatus2Cell(record: SysBill)
    {
        self.lblCodeBill.text = record.codeBill
        self.lblDistance.text = "( \(record.distance) Km)"
        self.lblShipperName.text = record.shipperName
    }
}
